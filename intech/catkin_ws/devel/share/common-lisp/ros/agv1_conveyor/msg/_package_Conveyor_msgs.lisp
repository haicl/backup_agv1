(cl:in-package agv1_conveyor-msg)
(cl:export '(CONVEYORF_RUN-VAL
          CONVEYORF_RUN
          CONVEYORF_REVERSE-VAL
          CONVEYORF_REVERSE
          CONVEYORB_RUN-VAL
          CONVEYORB_RUN
          CONVEYORB_REVERSE-VAL
          CONVEYORB_REVERSE
))