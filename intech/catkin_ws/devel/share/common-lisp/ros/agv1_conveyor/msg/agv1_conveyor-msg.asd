
(cl:in-package :asdf)

(defsystem "agv1_conveyor-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Conveyor_msgs" :depends-on ("_package_Conveyor_msgs"))
    (:file "_package_Conveyor_msgs" :depends-on ("_package"))
  ))