; Auto-generated. Do not edit!


(cl:in-package agv1_gpio-msg)


;//! \htmlinclude Speaker_msgs.msg.html

(cl:defclass <Speaker_msgs> (roslisp-msg-protocol:ros-message)
  ((mode
    :reader mode
    :initarg :mode
    :type cl:boolean
    :initform cl:nil)
   (run
    :reader run
    :initarg :run
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Speaker_msgs (<Speaker_msgs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Speaker_msgs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Speaker_msgs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name agv1_gpio-msg:<Speaker_msgs> is deprecated: use agv1_gpio-msg:Speaker_msgs instead.")))

(cl:ensure-generic-function 'mode-val :lambda-list '(m))
(cl:defmethod mode-val ((m <Speaker_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv1_gpio-msg:mode-val is deprecated.  Use agv1_gpio-msg:mode instead.")
  (mode m))

(cl:ensure-generic-function 'run-val :lambda-list '(m))
(cl:defmethod run-val ((m <Speaker_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv1_gpio-msg:run-val is deprecated.  Use agv1_gpio-msg:run instead.")
  (run m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Speaker_msgs>) ostream)
  "Serializes a message object of type '<Speaker_msgs>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'mode) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'run) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Speaker_msgs>) istream)
  "Deserializes a message object of type '<Speaker_msgs>"
    (cl:setf (cl:slot-value msg 'mode) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'run) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Speaker_msgs>)))
  "Returns string type for a message object of type '<Speaker_msgs>"
  "agv1_gpio/Speaker_msgs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Speaker_msgs)))
  "Returns string type for a message object of type 'Speaker_msgs"
  "agv1_gpio/Speaker_msgs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Speaker_msgs>)))
  "Returns md5sum for a message object of type '<Speaker_msgs>"
  "3022ae09200e24295c42c33070465986")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Speaker_msgs)))
  "Returns md5sum for a message object of type 'Speaker_msgs"
  "3022ae09200e24295c42c33070465986")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Speaker_msgs>)))
  "Returns full string definition for message of type '<Speaker_msgs>"
  (cl:format cl:nil "bool mode~%bool run~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Speaker_msgs)))
  "Returns full string definition for message of type 'Speaker_msgs"
  (cl:format cl:nil "bool mode~%bool run~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Speaker_msgs>))
  (cl:+ 0
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Speaker_msgs>))
  "Converts a ROS message object to a list"
  (cl:list 'Speaker_msgs
    (cl:cons ':mode (mode msg))
    (cl:cons ':run (run msg))
))
