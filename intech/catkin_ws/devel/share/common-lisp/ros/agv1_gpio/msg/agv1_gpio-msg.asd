
(cl:in-package :asdf)

(defsystem "agv1_gpio-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Led_msgs" :depends-on ("_package_Led_msgs"))
    (:file "_package_Led_msgs" :depends-on ("_package"))
    (:file "Safety_msgs" :depends-on ("_package_Safety_msgs"))
    (:file "_package_Safety_msgs" :depends-on ("_package"))
    (:file "Speaker_msgs" :depends-on ("_package_Speaker_msgs"))
    (:file "_package_Speaker_msgs" :depends-on ("_package"))
  ))