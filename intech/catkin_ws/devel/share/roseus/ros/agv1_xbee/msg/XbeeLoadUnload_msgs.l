;; Auto-generated. Do not edit!


(when (boundp 'agv1_xbee::XbeeLoadUnload_msgs)
  (if (not (find-package "AGV1_XBEE"))
    (make-package "AGV1_XBEE"))
  (shadow 'XbeeLoadUnload_msgs (find-package "AGV1_XBEE")))
(unless (find-package "AGV1_XBEE::XBEELOADUNLOAD_MSGS")
  (make-package "AGV1_XBEE::XBEELOADUNLOAD_MSGS"))

(in-package "ROS")
;;//! \htmlinclude XbeeLoadUnload_msgs.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass agv1_xbee::XbeeLoadUnload_msgs
  :super ros::object
  :slots (_header _status _enable _station_selection _case ))

(defmethod agv1_xbee::XbeeLoadUnload_msgs
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:status __status) "")
    ((:enable __enable) nil)
    ((:station_selection __station_selection) "")
    ((:case __case) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _status (string __status))
   (setq _enable __enable)
   (setq _station_selection (string __station_selection))
   (setq _case (round __case))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:enable
   (&optional (__enable :null))
   (if (not (eq __enable :null)) (setq _enable __enable)) _enable)
  (:station_selection
   (&optional __station_selection)
   (if __station_selection (setq _station_selection __station_selection)) _station_selection)
  (:case
   (&optional __case)
   (if __case (setq _case __case)) _case)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; string _status
    4 (length _status)
    ;; bool _enable
    1
    ;; string _station_selection
    4 (length _station_selection)
    ;; int8 _case
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; string _status
       (write-long (length _status) s) (princ _status s)
     ;; bool _enable
       (if _enable (write-byte -1 s) (write-byte 0 s))
     ;; string _station_selection
       (write-long (length _station_selection) s) (princ _station_selection s)
     ;; int8 _case
       (write-byte _case s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; string _status
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _status (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _enable
     (setq _enable (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; string _station_selection
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _station_selection (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int8 _case
     (setq _case (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _case 127) (setq _case (- _case 256)))
   ;;
   self)
  )

(setf (get agv1_xbee::XbeeLoadUnload_msgs :md5sum-) "76da4dc3ed4b4e3d47ad7c8dd9521bed")
(setf (get agv1_xbee::XbeeLoadUnload_msgs :datatype-) "agv1_xbee/XbeeLoadUnload_msgs")
(setf (get agv1_xbee::XbeeLoadUnload_msgs :definition-)
      "Header header

string status
bool enable
string station_selection
int8 case
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :agv1_xbee/XbeeLoadUnload_msgs "76da4dc3ed4b4e3d47ad7c8dd9521bed")


