;; Auto-generated. Do not edit!


(when (boundp 'agv1_followline::Followline_msgs)
  (if (not (find-package "AGV1_FOLLOWLINE"))
    (make-package "AGV1_FOLLOWLINE"))
  (shadow 'Followline_msgs (find-package "AGV1_FOLLOWLINE")))
(unless (find-package "AGV1_FOLLOWLINE::FOLLOWLINE_MSGS")
  (make-package "AGV1_FOLLOWLINE::FOLLOWLINE_MSGS"))

(in-package "ROS")
;;//! \htmlinclude Followline_msgs.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass agv1_followline::Followline_msgs
  :super ros::object
  :slots (_header _forward _backward ))

(defmethod agv1_followline::Followline_msgs
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:forward __forward) (make-array 16 :initial-element 0 :element-type :integer))
    ((:backward __backward) (make-array 16 :initial-element 0 :element-type :integer))
    )
   (send-super :init)
   (setq _header __header)
   (setq _forward __forward)
   (setq _backward __backward)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:forward
   (&optional __forward)
   (if __forward (setq _forward __forward)) _forward)
  (:backward
   (&optional __backward)
   (if __backward (setq _backward __backward)) _backward)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; int8[16] _forward
    (* 1    16)
    ;; int8[16] _backward
    (* 1    16)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; int8[16] _forward
     (dotimes (i 16)
       (write-byte (elt _forward i) s)
       )
     ;; int8[16] _backward
     (dotimes (i 16)
       (write-byte (elt _backward i) s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; int8[16] _forward
   (dotimes (i (length _forward))
     (setf (elt _forward i) (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> (elt _forward i) 127) (setf (elt _forward i) (- (elt _forward i) 256)))
     )
   ;; int8[16] _backward
   (dotimes (i (length _backward))
     (setf (elt _backward i) (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> (elt _backward i) 127) (setf (elt _backward i) (- (elt _backward i) 256)))
     )
   ;;
   self)
  )

(setf (get agv1_followline::Followline_msgs :md5sum-) "659069a15bfcad3113211bf07cd85a39")
(setf (get agv1_followline::Followline_msgs :datatype-) "agv1_followline/Followline_msgs")
(setf (get agv1_followline::Followline_msgs :definition-)
      "Header header

int8[16] forward
int8[16] backward
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :agv1_followline/Followline_msgs "659069a15bfcad3113211bf07cd85a39")


