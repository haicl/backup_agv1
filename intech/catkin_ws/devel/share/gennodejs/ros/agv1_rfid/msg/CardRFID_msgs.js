// Auto-generated. Do not edit!

// (in-package agv1_rfid.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class CardRFID_msgs {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.status = null;
      this.new_cardRFID = null;
      this.old_cardRFID = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('status')) {
        this.status = initObj.status
      }
      else {
        this.status = '';
      }
      if (initObj.hasOwnProperty('new_cardRFID')) {
        this.new_cardRFID = initObj.new_cardRFID
      }
      else {
        this.new_cardRFID = 0;
      }
      if (initObj.hasOwnProperty('old_cardRFID')) {
        this.old_cardRFID = initObj.old_cardRFID
      }
      else {
        this.old_cardRFID = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type CardRFID_msgs
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [status]
    bufferOffset = _serializer.string(obj.status, buffer, bufferOffset);
    // Serialize message field [new_cardRFID]
    bufferOffset = _serializer.int8(obj.new_cardRFID, buffer, bufferOffset);
    // Serialize message field [old_cardRFID]
    bufferOffset = _serializer.int8(obj.old_cardRFID, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type CardRFID_msgs
    let len;
    let data = new CardRFID_msgs(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [status]
    data.status = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [new_cardRFID]
    data.new_cardRFID = _deserializer.int8(buffer, bufferOffset);
    // Deserialize message field [old_cardRFID]
    data.old_cardRFID = _deserializer.int8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += _getByteLength(object.status);
    return length + 6;
  }

  static datatype() {
    // Returns string type for a message object
    return 'agv1_rfid/CardRFID_msgs';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2e1f9135a761a98dae7de78f5cbc1da8';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    string status
    int8 new_cardRFID
    int8 old_cardRFID
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new CardRFID_msgs(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.status !== undefined) {
      resolved.status = msg.status;
    }
    else {
      resolved.status = ''
    }

    if (msg.new_cardRFID !== undefined) {
      resolved.new_cardRFID = msg.new_cardRFID;
    }
    else {
      resolved.new_cardRFID = 0
    }

    if (msg.old_cardRFID !== undefined) {
      resolved.old_cardRFID = msg.old_cardRFID;
    }
    else {
      resolved.old_cardRFID = 0
    }

    return resolved;
    }
};

module.exports = CardRFID_msgs;
