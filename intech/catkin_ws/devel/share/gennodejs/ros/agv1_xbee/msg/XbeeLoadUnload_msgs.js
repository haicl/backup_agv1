// Auto-generated. Do not edit!

// (in-package agv1_xbee.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class XbeeLoadUnload_msgs {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.status = null;
      this.enable = null;
      this.station_selection = null;
      this.case = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('status')) {
        this.status = initObj.status
      }
      else {
        this.status = '';
      }
      if (initObj.hasOwnProperty('enable')) {
        this.enable = initObj.enable
      }
      else {
        this.enable = false;
      }
      if (initObj.hasOwnProperty('station_selection')) {
        this.station_selection = initObj.station_selection
      }
      else {
        this.station_selection = '';
      }
      if (initObj.hasOwnProperty('case')) {
        this.case = initObj.case
      }
      else {
        this.case = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type XbeeLoadUnload_msgs
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [status]
    bufferOffset = _serializer.string(obj.status, buffer, bufferOffset);
    // Serialize message field [enable]
    bufferOffset = _serializer.bool(obj.enable, buffer, bufferOffset);
    // Serialize message field [station_selection]
    bufferOffset = _serializer.string(obj.station_selection, buffer, bufferOffset);
    // Serialize message field [case]
    bufferOffset = _serializer.int8(obj.case, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type XbeeLoadUnload_msgs
    let len;
    let data = new XbeeLoadUnload_msgs(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [status]
    data.status = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [enable]
    data.enable = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [station_selection]
    data.station_selection = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [case]
    data.case = _deserializer.int8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += _getByteLength(object.status);
    length += _getByteLength(object.station_selection);
    return length + 10;
  }

  static datatype() {
    // Returns string type for a message object
    return 'agv1_xbee/XbeeLoadUnload_msgs';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '76da4dc3ed4b4e3d47ad7c8dd9521bed';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    string status
    bool enable
    string station_selection
    int8 case
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new XbeeLoadUnload_msgs(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.status !== undefined) {
      resolved.status = msg.status;
    }
    else {
      resolved.status = ''
    }

    if (msg.enable !== undefined) {
      resolved.enable = msg.enable;
    }
    else {
      resolved.enable = false
    }

    if (msg.station_selection !== undefined) {
      resolved.station_selection = msg.station_selection;
    }
    else {
      resolved.station_selection = ''
    }

    if (msg.case !== undefined) {
      resolved.case = msg.case;
    }
    else {
      resolved.case = 0
    }

    return resolved;
    }
};

module.exports = XbeeLoadUnload_msgs;
