# CMake generated Testfile for 
# Source directory: /home/intech/catkin_ws/src
# Build directory: /home/intech/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("agv1_base_controller")
subdirs("agv1_conveyor")
subdirs("agv1_followline")
subdirs("agv1_gpio")
subdirs("agv1_master")
subdirs("agv1_rfid")
subdirs("agv1_xbee")
subdirs("beginner_tutorials")
