#!/usr/bin/python3
import rospy
import roslib
import time
import serial
from enum import Enum, unique
from std_msgs.msg import String
from agv1_conveyor.msg import Conveyor_msgs
from agv1_xbee.msg import XbeeLoadUnload_msgs

@unique
class FunctionCode(Enum):
    CONVEYOR_B = "BT_1_DONE"
    CONVEYOR_F = "BT_2_DONE"
    ROBOT = "RB_DONE"

class Node_Xbee:
    def __init__(self):
        #Hardware init
        self.port = rospy.get_param("~port_xbee", '/dev/ttyUSB_XBEE')
        self.xbee = serial.Serial(self.port, baudrate = 9600, timeout=0.5)
 

        #Init ROS
        self.rate = rospy.get_param("~rate", 100)
        rospy.Subscriber('xbeeConveyor_topic', XbeeLoadUnload_msgs, self.xbeeConveryor_callback)
        
        self.xbeeConveyor_pub = rospy.Publisher('xbeeConveyor_topic', XbeeLoadUnload_msgs, queue_size = 10)
        self.msg_xbeeConveyor = XbeeLoadUnload_msgs()
        self.conveyor_pub = rospy.Publisher('conveyor_topic',Conveyor_msgs, queue_size=10)
        self.msg_conveyor = Conveyor_msgs()

        #Data
        self.buffData = []

        #Setting 
        self.dir_conveyorF = False
        self.dir_conveyorB = False

        self.flag_finish = 0
        self.flag_stopConveyorF = False
        self.flag_stopConveyorB = False
        self.countF = 0
        self.countB = 0
        self.time_stopConveyorF = 0   #dv: s
        self.time_stopConveyorB = 0   #dv: s
        self.timeExport = 0.4 # 0.4

        self.connectionROBOT = False
        self.flag_stopRobot = False
        self.countRobot = 0

        self.test = 0

    def openPort(self):
        """ Open port """
        if self.xbee.is_open == False:
            self.xbee.open()
        time.sleep(1)

    def closePort(self):
        """ Close port """
        self.xbee.reset_input_buffer()
        self.xbee.close()
        time.sleep(1)

    def xbeeConveryor_callback(self,data):
        """ Receve messages from topic xbeeConveyor_topic """
        if data.status == "busy" and data.enable == True:
            if data.station_selection == "shortConveyor":
                time.sleep(1)
                self.run_shortConveyor(data.case)
            elif data.station_selection == "longConveyor":
                time.sleep(1)
                self.run_longCoveyor(data.case)
            elif data.station_selection == "robot":
                time.sleep(1)
                self.run_robot(data.case)
            else:
                print("error message from topic xbeeConveyor")
        elif data.status == "finish" and data.enable == False:
            self.flag_busy = False
        else:
            print("error message from topic xbeeConveyor")

    def run_robot(self, case):
        """ Run robot """
        if case == 1:  #place
            if self.xbee.is_open == True:
                self.xbee.write(b'RB_1_1#')
                self.xbee.write(b'RB_1_1#')
        elif case == 2:   #pick
            if self.xbee.is_open == True:
                self.xbee.write(b'RB_1_2#')
                self.xbee.write(b'RB_1_2#')

        print("RUN ROBOT")
        self.connectionROBOT = True

    def run_shortConveyor(self,case):
        """ Run short Conveyor """
        if case == 1:
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_1#')
                self.conveyor_pub.publish(self.msg_conveyor)
                self.xbee.write(b'BT_N_1#')
                self.conveyor_pub.publish(self.msg_conveyor)

            self.time_stopConveyorF = 0   #dv: s
            self.time_stopConveyorB = 0   #dv: s
            print("input short")

        elif case == 2:
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF 
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB #
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_2#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 3:
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_3#')
                self.conveyor_pub.publish(self.msg_conveyor)
                self.xbee.write(b'BT_N_3#')
                self.conveyor_pub.publish(self.msg_conveyor)
                
            self.time_stopConveyorF = self.timeExport   #dv: s
            self.time_stopConveyorB = self.timeExport   #dv: s
            print("output short")
        elif case == 4:
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_4#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 5:
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_5#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 6:
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_6#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 7:
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB #
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_7#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 8:
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_N_8#')
                self.conveyor_pub.publish(self.msg_conveyor)
        else:
            print("error message from topic xbeeConveyor")

    def run_longCoveyor(self,case):
        """ Run long Conveyor """
        if case == 1:
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_1#')
                self.conveyor_pub.publish(self.msg_conveyor)
                self.xbee.write(b'BT_D_1#')
                self.conveyor_pub.publish(self.msg_conveyor)
            self.time_stopConveyorF = 0   #dv: s
            self.time_stopConveyorB = 0   #dv: s
            print("input long")

        elif case == 2:
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_2#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 3:
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_3#')
                self.conveyor_pub.publish(self.msg_conveyor)
                self.xbee.write(b'BT_D_3#')
                self.conveyor_pub.publish(self.msg_conveyor)
            self.time_stopConveyorF = self.timeExport + 0.1  #dv: s
            self.time_stopConveyorB = self.timeExport + 0.1  #dv: s
            print("output long")

        elif case == 4:
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_4#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 5:
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_5#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 6:
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_6#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 7:
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_7#')
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 8:
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.is_open == True:
                self.xbee.write(b'BT_D_8#')
                self.conveyor_pub.publish(self.msg_conveyor)
        else:
            print("error message from topic xbeeConveyor")

    def stopConveyor(self):
        """ Stop short Conveyor """
        # if self.flag_stopConveyorF == True and self.msg_conveyor.conveyorF_Run == True:
        #     self.flag_stopConveyorF = False
        #     if self.flag_finish == 0:
        #         self.msg_conveyor.conveyorF_Run = False
        #         self.msg_conveyor.conveyorB_Run = True
        #         self.conveyor_pub.publish(self.msg_conveyor)
        #         self.flag_finish += 1
        #     elif self.flag_finish == 1:
        #         self.msg_conveyor.conveyorF_Run = False
        #         self.msg_conveyor.conveyorB_Run = False
        #         self.conveyor_pub.publish(self.msg_conveyor)
                self.flag_finish += 1

            self.countF += 1
            if self.countF >= (self.time_stopConveyorF * self.rate):
                self.flag_stopConveyorF = False
                self.countF = 0
                self.flag_finish += 1
                self.msg_conveyor.conveyorF_Run = False
                self.conveyor_pub.publish(self.msg_conveyor)

                self.time_stopConveyorF = 0
        
        # if self.flag_stopConveyorB == True and self.msg_conveyor.conveyorB_Run == True:
        #     self.flag_stopConveyorB = False
        #     if self.flag_finish == 0:
        #         self.msg_conveyor.conveyorF_Run = True
        #         self.msg_conveyor.conveyorB_Run = False
        #         self.conveyor_pub.publish(self.msg_conveyor)   
        #         self.flag_finish += 1
        #     elif self.flag_finish == 1:
        #         self.msg_conveyor.conveyorF_Run = False
        #         self.msg_conveyor.conveyorB_Run = False
        #         self.conveyor_pub.publish(self.msg_conveyor)   
        #         self.flag_finish += 1

            self.countB += 1
            if self.countB >= (self.time_stopConveyorB * self.rate):
                self.flag_stopConveyorB = False
                self.countB = 0
                self.flag_finish += 1
                self.msg_conveyor.conveyorB_Run = False
                self.conveyor_pub.publish(self.msg_conveyor)   

                self.time_stopConveyorB = 0

    def stopRobot(self):
        if self.flag_stopRobot == True:
            self.countRobot += 1
            if self.countRobot >= (4 * self.rate):
                self.flag_stopRobot = False
                self.countRobot = 0

                self.msg_xbeeConveyor.enable = False
                self.msg_xbeeConveyor.status = "finish"
                self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)

    def readData(self):
        """ Read data from xbee """
        if self.xbee.is_open == True:
            if self.xbee.in_waiting > 0:
                data_xbee = self.xbee.read(1).decode('ASCII') 

                if data_xbee == "#":
                    functionCode_conveyor = "".join(self.buffData[len(self.buffData) - len(FunctionCode.CONVEYOR_B.value):])
                    functionCode_robot = "".join(self.buffData[len(self.buffData) - len(FunctionCode.ROBOT.value):])

                    # Conveyor
                    if functionCode_conveyor == FunctionCode.CONVEYOR_B.value:
                        self.flag_stopConveyorB = True
                    elif functionCode_conveyor == FunctionCode.CONVEYOR_F.value:
                        self.flag_stopConveyorF = True

                    # Robot
                    if  functionCode_robot == FunctionCode.ROBOT.value:
                        self.xbee.write((str(functionCode_robot) + "_ACK#").encode('utf-8'))
                        if self.connectionROBOT == True:
                            self.flag_stopRobot = True
                            self.connectionROBOT = False

                    self.buffData.clear()
                else:
                    self.buffData.append(data_xbee)
        else:
            self.openPort()    

    #############################
    #           MAIN            #
    #############################
    def main(self):
        self.readData()

        self.stopConveyor()
        if self.flag_finish >= 2:
            self.flag_finish = 0
            self.flag_stopConveyorF = False
            self.flag_stopConveyorB = False
            #Dung bang tai tren lung
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = False
            self.conveyor_pub.publish(self.msg_conveyor)  
            
            self.msg_xbeeConveyor.status = "finish"
            self.msg_xbeeConveyor.enable = False
            self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
                
        self.stopRobot()

        ## TEST CONVEYOR
        # if self.flag_stopConveyorB == True or self.flag_stopConveyorF == True:
        #     self.msg_conveyor.conveyorF_Run = False
        #     self.msg_conveyor.conveyorB_Run = False
        #     self.conveyor_pub.publish(self.msg_conveyor)

        #     self.flag_stopConveyorB = False
        #     self.flag_stopConveyorF = False
        #     self.test += 1
        #     print(self.test)

        # if self.flag_finish >= 2:
        #     self.flag_finish = 0
        #     self.test += 1
        #     print(self.test)

        # if self.test == 0:
        #     time.sleep(2)
        #     self.run_shortConveyor(1)
        #     self.test = 1
        # elif self.test == 2:
        #     time.sleep(2)
        #     self.run_shortConveyor(3)
        #     self.test = 3
        
        # if self.test == 4:
        #     self.test = 0

if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_XBEE')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    agv1_xbee = Node_Xbee()
    try:
        rate = rospy.Rate(agv1_xbee.rate) 
        while not rospy.is_shutdown():
            agv1_xbee.main()
            rate.sleep()

    except rospy.ROSInternalException:
        print("\n Node Xbee ERROR \n")
    finally:
        agv1_xbee.closePort()