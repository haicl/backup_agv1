import board
import neopixel
import rospy

def test_led():
    pixels = neopixel.NeoPixel(board.D18, 10)
    pixels[0] = (255, 0, 0)

if __name__ == '__main__':
    try:
        test_led()
    except rospy.ROSInterruptException:
        pass
