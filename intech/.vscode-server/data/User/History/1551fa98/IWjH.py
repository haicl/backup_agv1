#!/usr/bin/python3
import rospy
import roslib
import time
from std_msgs.msg import String
from agv1_followline.msg import Followline_msgs
from agv1_rfid.msg import CardRFID_msgs
from agv1_xbee.msg import XbeeLoadUnload_msgs
from geometry_msgs.msg import Twist
from enum import Enum, unique
from agv1_gpio.msg import Speaker_msgs

@unique
class Turn(Enum):
    TURN_LEFT = 1
    TURN_CENTER = 2
    TURN_RIGHT = 3

@unique
class Direction(Enum):
    FORWARD = 1
    BACKWARD = 2

@unique
class FuntionCard(Enum):
    DEC_CARD = 10
    ACC_CARD = 11

    # LONG_CONVEYOR1 = 1 
    # LONG_CONVEYOR2 = 2
    # LONG_CONVEYOR3 = 3
    # SHORT_CONVEYOR1 = 4 
    # SHORT_CONVEYOR2 = 5
    # SHORT_CONVEYOR3 = 6

    # ROBOT_POINT_BOT = 7
    # ROBOT_POINT_CENTER = 8
    # ROBOT_POINT_TOP = 9

    SHORT_CONVEYOR = 1 
    ROBOT = 2
    LONG_CONVEYOR = 3 

@unique
class Load_Unload(Enum):
    LOAD = 1
    UNLOAD = 3

@unique
class Process(Enum):
    STEP1 = 1
    STEP2 = 2

class Node_Master():
    def __init__(self):
        #Hardware Followline
        self.number_eyes = 16
        self.eyeDistance = 140

        #PID
        self.kp = 0.00056 #0.00061
        self.ki = 0.0
        self.kd = 0.0 #0.0026
        self.error = 0
        self.sumError = 0
        self.preError = 0
        self.setPoint = ((self.number_eyes - 1) * self.eyeDistance) / 2 

        #Controller
        self.linear_vel = 0.14          # m/s
        self.max_angle_vel = 1.0        # radian/s
        self.min_angle_vel = -1.0       # radian/s
        self.min_linear_vel = 0.14
        self.max_linear_vel = 0.36

        #Data
        self.direction = 1
        self.turn = 2 
        self.rfid_data = 0
        self.fl_backward = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.fl_forward = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.count_outLine = 0
        self.time_outLine = 1   # dv: s

        #ROS
        self.rate = 20
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
        self.move_cmd = Twist()
        self.move_cmd.linear.x = 0
        self.move_cmd.linear.y = 0
        self.move_cmd.linear.z = 0
        self.move_cmd.angular.x = 0
        self.move_cmd.angular.y = 0
        self.move_cmd.angular.z = 0
        rospy.Subscriber('followline_topic', Followline_msgs, self.followline_callback)
        rospy.Subscriber('rfid_topic', CardRFID_msgs, self.rfid_callback)
        rospy.Subscriber('safety_topic', String, self.safety_callback)
        rospy.Subscriber('xbeeConveyor_topic', XbeeLoadUnload_msgs, self.xbeeConveryor_callback)

        self.xbeeConveyor_pub = rospy.Publisher('xbeeConveyor_topic', XbeeLoadUnload_msgs, queue_size = 10)
        self.msg_xbeeConveyor = XbeeLoadUnload_msgs()
        self.speaker_pub = rospy.Publisher('speaker_topic', Speaker_msgs, queue_size = 10)
        self.msg_speaker = Speaker_msgs()

        #Setting
        self.stop_or_run = True
        self.process = 0
        self.timeDelay = 5 #dv:s
        self.flag_safety = 0
        self.flag_status_speaker = 0

    def followline_callback(self, data):
        """ Recive messages from topic followline_topic """
        for i in range(len(data.backward)):
            self.fl_backward[i] = data.backward[i]
        for i in range(len(data.forward)):
            self.fl_forward[i] = data.forward[i]

    def rfid_callback(self, data):
        """ Receve messages from topic rfid_topic """
        if data.status == "success":
            self.rfid_data = data.new_cardRFID

            # Tang giam toc
            if self.rfid_data == FuntionCard.DEC_CARD.value:
                if self.process == 2 or self.process == 7:
                    if self.linear_vel == self.max_linear_vel:
                        print("GIAM TOC")
                        self.linear_vel = self.min_linear_vel
                    else:
                        print("TANG TOC")
                        self.linear_vel = self.max_linear_vel
                # else:
                #     print("GIAM TOC")
                #     self.linear_vel = self.min_linear_vel
            # if self.rfid_data == FuntionCard.DEC_CARD.value:
            #     self.linear_vel = self.min_linear_vel
            # elif self.rfid_data == FuntionCard.ACC_CARD.value:
            #     self.linear_vel = self.max_linear_vel

            # DEMO
            # elif (self.rfid_data == FuntionCard.LONG_CONVEYOR1.value or self.rfid_data == FuntionCard.LONG_CONVEYOR2.value or self.rfid_data == FuntionCard.LONG_CONVEYOR3.value):
            #     self.process_longConveyor()
            # elif (self.rfid_data == FuntionCard.SHORT_CONVEYOR1.value or self.rfid_data == FuntionCard.SHORT_CONVEYOR2.value or self.rfid_data == FuntionCard.SHORT_CONVEYOR3.value):
            #     self.process_shortConveyor()
            # elif (self.rfid_data == FuntionCard.ROBOT_POINT_BOT.value or self.rfid_data == FuntionCard.ROBOT_POINT_CENTER.value or self.rfid_data == FuntionCard.ROBOT_POINT_TOP.value):
            #     self.process_robot()
            
            # 6x6 exibition
            if (self.rfid_data == FuntionCard.LONG_CONVEYOR.value and data.new_cardRFID != data.old_cardRFID):
                self.process_longConveyor()
                print(self.process)
            elif (self.rfid_data == FuntionCard.SHORT_CONVEYOR.value and data.new_cardRFID != data.old_cardRFID):
                self.process_shortConveyor()
                print(self.process)
            # elif (self.rfid_data == FuntionCard.ROBOT.value):
            #     self.process_robot()

    #region : CODE FOR EXHIBITION 
    def process_longConveyor(self):
        if self.process == 0:
            self.stop_or_run = False
            self.process = 1
            self.direction = Direction.BACKWARD.value
            time.sleep(2)
            self.stop_or_run = True

        # if self.process == 1:       #t3-process1
        #     self.stop_or_run = False
        #     self.msg_xbeeConveyor.status = "busy"
        #     self.msg_xbeeConveyor.enable = True
        #     self.msg_xbeeConveyor.station_selection = "longConveyor"
        #     self.msg_xbeeConveyor.case = 1
        #     self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
        #     self.process += 1
        #     self.direction = Direction.BACKWARD.value

        # elif self.process == 4:     #t3-process4
        #     self.stop_or_run == False
        #     self.msg_xbeeConveyor.status = "busy"
        #     self.msg_xbeeConveyor.enable = True
        #     self.msg_xbeeConveyor.station_selection = "longConveyor"
        #     self.msg_xbeeConveyor.case = 1
        #     self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
        #     #Vao ham doi 5s de chuyen chu trinh
        #     self.process = 10

        # elif self.process == 7:     #t3-process7
        #     self.stop_or_run == False
        #     self.msg_xbeeConveyor.status = "busy"
        #     self.msg_xbeeConveyor.enable = True
        #     self.msg_xbeeConveyor.station_selection = "longConveyor"
        #     self.msg_xbeeConveyor.case = 3
        #     self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
        #     self.process += 1
        #     self.direction = Direction.BACKWARD.value
    
    def process_shortConveyor(self):
        if self.process == 1:       #t1-process2
            self.stop_or_run = False
        #     self.msg_xbeeConveyor.status = "busy"
        #     self.msg_xbeeConveyor.enable = True
        #     self.msg_xbeeConveyor.station_selection = "shortConveyor"
        #     self.msg_xbeeConveyor.case = 3
        #     self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
            self.process = 0
            self.direction = Direction.FORWARD.value
            time.sleep(2)
            self.stop_or_run = True

        # elif self.process == 6:       #t1-process2
        #     self.stop_or_run = False
        #     self.msg_xbeeConveyor.status = "busy"
        #     self.msg_xbeeConveyor.enable = True
        #     self.msg_xbeeConveyor.station_selection = "shortConveyor"
        #     self.msg_xbeeConveyor.case = 1
        #     self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
        #     self.process += 1
        #     self.direction = Direction.FORWARD.value

        # if self.process == 9:
        #     self.stop_or_run = False
        #     time.sleep(self.timeDelay)
        #     self.stop_or_run = True
        #     self.process = 0
        #     self.direction = Direction.FORWARD.value

    # def process_robot(self):
    #     if self.process == 0:
    #         # self.stop_or_run = False
    #         # self.msg_xbeeConveyor.status = "busy"
    #         # self.msg_xbeeConveyor.enable = True
    #         # self.msg_xbeeConveyor.station_selection = "robot"
    #         # self.msg_xbeeConveyor.case = 1
    #         # self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1

    #     elif self.process == 3:
    #         # self.stop_or_run = False
    #         # self.msg_xbeeConveyor.status = "busy"
    #         # self.msg_xbeeConveyor.enable = True
    #         # self.msg_xbeeConveyor.station_selection = "robot"
    #         # self.msg_xbeeConveyor.case = 1
    #         # self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1

    #     elif self.process == 5:
    #         # self.stop_or_run = False
    #         # self.msg_xbeeConveyor.status = "busy"
    #         # self.msg_xbeeConveyor.enable = True
    #         # self.msg_xbeeConveyor.station_selection = "robot"
    #         # self.msg_xbeeConveyor.case = 2
    #         # self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1

    #     elif self.process == 8:
    #         # self.stop_or_run = False
    #         # self.msg_xbeeConveyor.status = "busy"
    #         # self.msg_xbeeConveyor.enable = True
    #         # self.msg_xbeeConveyor.station_selection = "robot"
    #         # self.msg_xbeeConveyor.case = 2
    #         # self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #endregion

    #region : CODE DEMO FOR ROOM R&D
    # def process_longConveyor(self):
    #     if self.process == 0 and self.rfid_data == 2:       #V1_T2
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "longConveyor"
    #         self.msg_xbeeConveyor.case = 4
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 2 and self.rfid_data == 2: #V2_T2
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "longConveyor"
    #         self.msg_xbeeConveyor.case = 4
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 5 and self.rfid_data == 1: #V3_T1
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "longConveyor"
    #         self.msg_xbeeConveyor.case = 6
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 6 and self.rfid_data == 3: #V3_T3
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "longConveyor"
    #         self.msg_xbeeConveyor.case = 8
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 8 and self.rfid_data == 1: #V4_T1
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "longConveyor"
    #         self.msg_xbeeConveyor.case = 6
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 9 and self.rfid_data == 3: #V4_T3
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "longConveyor"
    #         self.msg_xbeeConveyor.case = 8
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1

    # def process_shortConveyor(self):
    #     if self.process == 1 and self.rfid_data == 5:   #V1_T5
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "shortConveyor"
    #         self.msg_xbeeConveyor.case = 2
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 3 and self.rfid_data == 4: #V2_T4
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "shortConveyor"
    #         self.msg_xbeeConveyor.case = 5
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 4 and self.rfid_data == 6: #V2_T6
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "shortConveyor"
    #         self.msg_xbeeConveyor.case = 7
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 7 and self.rfid_data == 5: #V3_T5
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "shortConveyor"
    #         self.msg_xbeeConveyor.case = 2
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 10 and self.rfid_data == 4: #V4_T4
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "shortConveyor"
    #         self.msg_xbeeConveyor.case = 5
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process += 1
    #     elif self.process == 11 and self.rfid_data == 6: #V4_T6
    #         self.stop_or_run = False
    #         time.sleep(1)
    #         self.msg_xbeeConveyor.status = "busy"
    #         self.msg_xbeeConveyor.enable_conveyor = True
    #         self.msg_xbeeConveyor.conveyor_selection = "shortConveyor"
    #         self.msg_xbeeConveyor.case = 7
    #         self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
    #         self.process = 0 
    #endregion

    def xbeeConveryor_callback(self,data):
        if data.status == "finish":
            if self.process != 10:
                time.sleep(1)
                self.stop_or_run = True
            elif self.process == 10 :
                self.stop_or_run = False
                time.sleep(self.timeDelay)
                self.msg_xbeeConveyor.status = "busy"
                self.msg_xbeeConveyor.enable = True
                self.msg_xbeeConveyor.station_selection = "longConveyor"
                self.msg_xbeeConveyor.case = 3
                self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)
                self.direction = Direction.BACKWARD.value
                self.process = 5
        else:
            self.stop_or_run = False

    def constrain(self, value, min, max):
        """ Constrains a number to be within a range. """
        if value < max and value > min:
            return value
        elif value > max:
            return max
        else:
            return min

    def calc_error(self):
        """ Caculate error """
        avg = 0  # this is for the weighted total
        sum = 0  # this is for the denominator
        #Forward
        if self.direction == Direction.FORWARD.value:
            if self.turn == Turn.TURN_CENTER.value:
                for i in range(self.number_eyes):
                    if self.fl_forward[i] > 0:
                        avg += i*self.eyeDistance
                        sum += 1
            elif self.turn == Turn.TURN_RIGHT.value:
                for i in reversed(range(0, self.number_eyes)):
                    if self.fl_forward[i] > 0:
                        avg = (4*i - 6) * self.eyeDistance
                        sum = 4
                        break
            elif self.turn == Turn.TURN_LEFT.value:
                for i in range(self.number_eyes):
                    if self.fl_forward[i] > 0:
                        avg = (4*i + 6) * self.eyeDistance
                        sum = 4
                        break
            else:
                pass
        #Backward
        elif self.direction == Direction.BACKWARD.value:
            if self.turn == Turn.TURN_CENTER.value:
                for i in range(self.number_eyes):
                    if self.fl_backward[i] > 0:
                        avg += i*self.eyeDistance
                        sum += 1
            elif self.turn == Turn.TURN_RIGHT.value:
                for i in reversed(range(0, self.number_eyes)):
                    if self.fl_backward[i] > 0:
                        avg = (4*i - 6) * self.eyeDistance
                        sum = 4
                        break
            elif self.turn == Turn.TURN_LEFT.value:
                for i in range(self.number_eyes):
                    if self.fl_backward[i] > 0:
                        avg = (4*i + 6) * self.eyeDistance
                        sum = 4
                        break
            else:
                pass
        #Unknow
        else:
            pass

        #Caculate error
        if sum == 0:
            self.error = -1
        else:
            self.error = (avg / sum) - self.setPoint

    def pid_control(self):
        """ Caculate PID """
        if self.error != -1:
            Up = self.kp * self.error
            Ui = self.ki * (self.sumError + self.error)
            Ud = self.kd * (self.error - self.preError)
            PID = Up + Ui + Ud

            self.preError = self.error
            self.sumError += self.error

            return PID
        else:
            return -1.0

    def stopAGV(self):
        """ Stop AGV """
        self.move_cmd.linear.x = 0
        self.move_cmd.linear.y = 0
        self.move_cmd.linear.z = 0

        self.move_cmd.angular.x = 0
        self.move_cmd.angular.y = 0
        self.move_cmd.angular.z = 0
        self.cmd_vel_pub.publish(self.move_cmd)

    def runAGV(self):
        self.calc_error()
        angle_speed = self.pid_control()
        if angle_speed != -1:
            angle_speed = self.constrain(angle_speed, self.min_angle_vel, self.max_angle_vel)
            
            # Toc do tinh tien
            if self.direction == Direction.BACKWARD.value:
                self.move_cmd.linear.x = -self.linear_vel
            else:
                self.move_cmd.linear.x = self.linear_vel
            
            # Toc do goc
            if self.error < self.setPoint:
                self.move_cmd.angular.z =  - angle_speed
            elif self.error > self.setPoint:
                self.move_cmd.angular.z = angle_speed
            else:
                self.move_cmd.angular.z = 0
            self.count_outLine = 0 
        else:
            self.count_outLine += 1
            if self.count_outLine > (self.time_outLine * self.rate):
                self.stopAGV()

        self.cmd_vel_pub.publish(self.move_cmd)

    #hai
    def safety_callback(self,data):
        if data.data == "deceleration_agv":
            self.flag_safety = 1
            self.max_linear_vel = 0.2
        elif data.data == "stop_agv":
            self.flag_safety = 2
        else:
            self.flag_safety = 0
        
              
    #############################
    #           MAIN            #
    #############################
    def main(self):
        ###process safety
        # if self.flag_safety < 2:
        #     if self.stop_or_run == True:
        #         self.runAGV()
        #     else:
        #         self.stopAGV()
            
            # if self.flag_status_speaker == 1 or self.flag_status_speaker == 0:
            #     self.msg_speaker.run = True
            #     self.msg_speaker.mode = False
            #     self.speaker_pub.publish(self.msg_speaker)
        #     #     self.flag_status_speaker = 2
        # else:
        #     self.stopAGV()
            # if self.flag_status_speaker == 2 or self.flag_status_speaker == 0:
            #     self.msg_speaker.run = True
            #     self.msg_speaker.mode = True
            #     self.speaker_pub.publish(self.msg_speaker)
            #     self.flag_status_speaker = 1

        ##### end safety
        if self.stop_or_run == True:
            self.runAGV()
        else:
            self.stopAGV()


if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_MASTER',disable_signals= True)
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    agv1_master = Node_Master()
    time.sleep(1)
    try:
        rate = rospy.Rate(agv1_master.rate) 
        while not rospy.is_shutdown():
            agv1_master.main()
            rate.sleep()

    except rospy.ROSInternalException:
        print("\n Node Master ERROR \n")
    finally:
        agv1_master.stopAGV()
        rospy.signal_shutdown("Shutdown node AGV1_MASTER")