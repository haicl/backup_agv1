#!/usr/bin/python3
import gpiozero
import roslib
import rospy
import time
from agv1_conveyor.msg import Conveyor_msgs

class Node_Load_Unload:
    def __init__(self):
        # Init hardware conveyor
        self.pin_Run_ConveyorF = 26
        self.pin_Reverse_ConveyorF = 19
        self.pin_Run_ConveyorB = 13
        self.pin_Reverse_ConveyorB = 6
        self.run_conveyorF = gpiozero.OutputDevice(self.pin_Run_ConveyorF)
        self.reverse_conveyorF = gpiozero.OutputDevice(self.pin_Reverse_ConveyorF)
        self.run_conveyorB = gpiozero.OutputDevice(self.pin_Run_ConveyorB)
        self.reverse_conveyorB = gpiozero.OutputDevice(self.pin_Reverse_ConveyorB)
        
        # ROS Init
        rospy.Subscriber("conveyor_topic", Conveyor_msgs, self.conveyor_callback)
        self.rate = rospy.get_param("~rate", 20)

    def conveyor_callback(self,data):
        """ Recive messages from topic load_unload """
        # Conveyor forward
        if data.conveyorF_Run == True:
            self.run_conveyorF.on()
            if data.conveyorF_Reverse == True:
                self.reverse_conveyorF.off()
            else:
                self.reverse_conveyorF.on()
        else:
            self.run_conveyorF.off()

        # Conveyor backward
        if data.conveyorB_Run == True:
            self.run_conveyorB.on()
            if data.conveyorB_Reverse == True:
                self.reverse_conveyorB.on()
            else:
                self.reverse_conveyorB.off()
        else:
            self.run_conveyorB.off()

    def stopConveyor(self):
        self.run_conveyorB.off()
        self.run_conveyorF.off()
        self.reverse_conveyorB.off()
        self.reverse_conveyorF.off()

if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_CONVEYOR')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    node_load_unload = Node_Load_Unload()
    node_load_unload.stopConveyor()
    rate = rospy.Rate(node_load_unload.rate)
    try:
        while not rospy.is_shutdown():
            rate.sleep()
    except rospy.ROSInterruptException:
        print("NODE AGV CONVEYOR ERROR")
    finally:
        node_load_unload.stopConveyor()