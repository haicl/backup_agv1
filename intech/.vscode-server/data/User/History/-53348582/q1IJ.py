#!/usr/bin/env python3
from cgi import test
import cmd
import imp
import gpiozero
import smbus
import rospy
import time 
import math
from geometry_msgs.msg import Twist
from std_msgs.msg import String
from agv1_load_unload.msg import Conveyor_msgs

class BaseController_AGV1():
    def __init__(self):
        # Init Driver Motor
        self.addI2c_leftMotor = 0x61
        self.addI2c_rightMotor = 0x60
        self.reg_write_dac = 0x40
        self.i2cBus = smbus.SMBus(1)

        #-pin name leftMotor- = GPIO
        self.pin_SS_leftMotor = 8
        self.pin_CW_leftMotor = 1
        self.pin_AR_leftMotor = 7
        self.pin_ERR_leftMotor = 25
        self.starStop_leftMotor = gpiozero.OutputDevice(self.pin_SS_leftMotor)
        self.reverse_leftMotor = gpiozero.OutputDevice(self.pin_CW_leftMotor)
        self.alarmReset_leftMotor = gpiozero.OutputDevice(self.pin_AR_leftMotor)
        self.error_leftMotor = gpiozero.InputDevice(self.pin_ERR_leftMotor, pull_up=True)
        #--------------------------------------------------------------------------------

        #-pin name rightMotor- = GPIO
        self.pin_SS_rightMotor = 16
        self.pin_CW_rightMotor = 20
        self.pin_AR_rightMotor = 21
        self.pin_ERR_rightMotor = 12
        self.starStop_rightMotor = gpiozero.OutputDevice(self.pin_SS_rightMotor)
        self.reverse_rightMotor = gpiozero.OutputDevice(self.pin_CW_rightMotor)
        self.alarmReset_rightMotor = gpiozero.OutputDevice(self.pin_AR_rightMotor)
        self.error_rightMotor = gpiozero.InputDevice(self.pin_ERR_rightMotor, pull_up=True)
        #----------------------------------------------------------------------------------

        #--pin brake-- = GPIO
        self.pin_BRAKE = 24
        self.brakeAGV = gpiozero.OutputDevice(self.pin_BRAKE)
        #----------------------------------------------------

        # Init configuration AGV
        self.wheel_Base = 0.480 # 480mm <-> 0.48m
        self.max_vel = 4095
        self.min_vel = 0

        # Init ROS
        self.nameTopic_cmd_vel = rospy.get_param("~nameTopic_cmd_vel", 'cmd_vel')
        rospy.Subscriber(self.nameTopic_cmd_vel, Twist, self.twistCallback)
    
        # Control Value
        self.speedLeftMotor = 0
        self.speedRightMotor = 0
        self.ADC_LeftMotor = [0, 0]
        self.ADC_RightMotor = [0, 0]

        # Cmd_vel
        self.dx = 0
        self.dr = 0

        # Time
        self.rate = 20
        self.time_delay = 0.2
        
    def twistCallback(self, cmdVel):
        """ Recive messages from topic cmd_cel """
        self.dx = cmdVel.linear.x
        self.dr = cmdVel.angular.z

    ############################
    #           MAIN           #
    ############################
    def main(self):
        # Calculater the speed of the 2 wheels 
        self.speedRightMotor = 1.0 * self.dx + self.dr * self.wheel_Base / 2
        self.speedLeftMotor = 1.0 * self.dx - self.dr * self.wheel_Base / 2
        
        self.calc_ADC_Moror()
        self.setSpeed_leftMotor()
        self.setSpeed_rightMotor()

    def calc_ADC_Moror(self):
        """ Convert speed from m/s to ADC """
        # aX = Y; 
        # X: value -> voltage (from 0 to 4095)
        # Y: speed (m/s)
        # Thuc nghiem => a = 0.000178

        voltage_leftMotor = self.speedLeftMotor / 0.000178
        if abs(voltage_leftMotor) > self.max_vel:
            self.ADC_LeftMotor[0] = (self.max_vel & 0xff0) >> 4 
            self.ADC_LeftMotor[1] = (self.ADC_LeftMotor[1] & 0xf) << 4
        elif abs(voltage_leftMotor) < self.min_vel:
            self.ADC_LeftMotor[0] = (self.min_vel & 0xff0) >> 4 
            self.ADC_LeftMotor[1] = (self.ADC_LeftMotor[1] & 0xf) << 4
        else: 
            self.ADC_LeftMotor[0] = (int(abs(voltage_leftMotor)) & 0xff0) >> 4 
            self.ADC_LeftMotor[1] = (self.ADC_LeftMotor[1] & 0xf) << 4

        voltage_rightMotor = self.speedRightMotor / 0.000178
        if abs(voltage_rightMotor) > self.max_vel:
            self.ADC_RightMotor[0] = (self.max_vel & 0xff0) >> 4 
            self.ADC_RightMotor[1] = (self.ADC_RightMotor[1] & 0xf) << 4
        elif abs(voltage_rightMotor) < self.min_vel:
            self.ADC_RightMotor[0] = (self.min_vel & 0xff0) >> 4 
            self.ADC_RightMotor[1] = (self.ADC_RightMotor[1] & 0xf) << 4
        else:
            self.ADC_RightMotor[0] = (int(abs(voltage_rightMotor)) & 0xff0) >> 4 
            self.ADC_RightMotor[1] = (self.ADC_RightMotor[1] & 0xf) << 4

    def setSpeed_leftMotor(self):
        """ Set speed left motor  """
        if self.speedLeftMotor < 0:
            self.reverse_leftMotor.on()
        else:
            self.reverse_leftMotor.off()

        self.i2cBus.write_i2c_block_data(self.addI2c_leftMotor, self.reg_write_dac, self.ADC_LeftMotor)

    def setSpeed_rightMotor(self):
        """ Set speed right motor  """
        if self.speedRightMotor < 0:
            self.reverse_rightMotor.off()
        else:
            self.reverse_rightMotor.on()

        self.i2cBus.write_i2c_block_data(self.addI2c_rightMotor, self.reg_write_dac, self.ADC_RightMotor)

    def runMotor(self):
        """ Run Motor """
        self.starStop_leftMotor.off()
        self.starStop_rightMotor.off()
        time.sleep(self.time_delay)
        self.brakeAGV.off()

    def stopMotor(self):
        """ Stop Motor """
        self.starStop_leftMotor.on()
        self.starStop_rightMotor.on()
        time.sleep(self.time_delay)
    
    def brakeMotor(self):
        """ Brake Motor AGV """
        self.starStop_leftMotor.on()
        self.starStop_rightMotor.on()
        time.sleep(self.time_delay)
        self.brakeAGV.on()

    def alarmReset(self):
        """ Reset Alarm Motor """
        self.alarmReset_leftMotor.off()
        self.alarmReset_rightMotor.off()
        time.sleep(self.time_delay)
        self.alarmReset_leftMotor.on()
        self.alarmReset_rightMotor.on()

    def initialMotor(self):
        """ Initial Motor AGV """
        self.stopMotor()
        self.i2cBus.write_i2c_block_data(self.addI2c_leftMotor, self.reg_write_dac, self.ADC_LeftMotor)
        self.i2cBus.write_i2c_block_data(self.addI2c_rightMotor, self.reg_write_dac, self.ADC_RightMotor)
        self.alarmReset()
        time.sleep(self.time_delay)
        self.runMotor()


class GPIO_AGV1():
    def __init__(self):
        # Init hardware conveyor
        self.pin_Run_ConveyorF = 26
        self.pin_Reverse_ConveyorF = 19
        self.pin_Run_ConveyorB = 13
        self.pin_Reverse_ConveyorB = 6

        self.run_conveyorF = gpiozero.OutputDevice(self.pin_Run_ConveyorF)
        self.reverse_conveyorF = gpiozero.OutputDevice(self.pin_Reverse_ConveyorF)
        self.run_conveyorB = gpiozero.OutputDevice(self.pin_Run_ConveyorB)
        self.reverse_conveyorB = gpiozero.OutputDevice(self.pin_Reverse_ConveyorB)

        #Init ROS
        self.nameTopic_coveyor = "Conveyor"
        rospy.Subscriber(self.nameTopic_coveyor, Conveyor_msgs, self.conveyor_callback)


    def initConveyor(self):
        """ Funtion Init Conveyor """
        self.run_conveyorF.on()
        self.reverse_conveyorF.on()

        self.run_conveyorB.on()
        self.reverse_conveyorB.on()

    def conveyor_callback(self, data):
        """ Recive messages from topic Conveyor """
        # Conveyor forward
        print("ok")
        if data.conveyorF_Run == True:
            self.run_conveyorF.off()
            if data.conveyorF_Reverse == True:
                self.reverse_conveyorF.off()
            else:
                self.reverse_conveyorF.on()
        else:
            self.run_conveyorF.on()

        # Conveyor backward
        if data.conveyorB_Run == True:
            self.run_conveyorB.off()
            if data.conveyorB_Reverse == True:
                self.reverse_conveyorB.off()
            else:
                self.reverse_conveyorB.on()
        else:
            self.run_conveyorB.on()



if __name__ == '__main__':
    """ main """
    # Init NODE
    rospy.init_node('AGV1_GPIO_AND_BASECONTROLLER', disable_signals=True)
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    baseController = BaseController_AGV1()
    gpio_agv = GPIO_AGV1()
    try:
        baseController.initialMotor()
        gpio_agv.initConveyor()
        r = rospy.Rate(baseController.rate)
        while not rospy.is_shutdown():
            baseController.main()
            r.sleep()

    except rospy.ROSInterruptException:
        print("\n Basecontroller ERROR \n")
    finally:
        baseController.stopMotor()
        rospy.signal_shutdown("Shutdown node AGV1_GPIO_AND_BASECONTROLLER")

