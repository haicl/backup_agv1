#!/usr/bin/python3
from asyncore import read
from string import printable
from urllib.request import install_opener
import gpiozero
import roslib
import rospy
import time
import serial
import io
import codecs
from agv1_conveyor.msg import Conveyor_msgs
from agv1_gpio.msg import Led_msgs
from agv1_gpio.msg import Speaker_msgs
from std_msgs.msg import String

class Conveyor:
    def __init__(self):
        # Init hardware conveyor
        self.pin_Run_ConveyorF = 26
        self.pin_Reverse_ConveyorF = 19
        self.pin_Run_ConveyorB = 13
        self.pin_Reverse_ConveyorB = 6
        self.run_conveyorF = gpiozero.OutputDevice(self.pin_Run_ConveyorF)
        self.reverse_conveyorF = gpiozero.OutputDevice(self.pin_Reverse_ConveyorF)
        self.run_conveyorB = gpiozero.OutputDevice(self.pin_Run_ConveyorB)
        self.reverse_conveyorB = gpiozero.OutputDevice(self.pin_Reverse_ConveyorB)
        
        # ROS Init
        rospy.Subscriber("conveyor_topic", Conveyor_msgs, self.conveyor_callback)
        self.rate = rospy.get_param("~rate", 5)

    def conveyor_callback(self,data):
        """ Recive messages from topic load_unload """
        # Conveyor forward
        if data.conveyorF_Run == True:
            self.run_conveyorF.on()
            if data.conveyorF_Reverse == True:
                self.reverse_conveyorF.off()
            else:
                self.reverse_conveyorF.on()
        else:
            self.run_conveyorF.off()

        # Conveyor backward
        if data.conveyorB_Run == True:
            self.run_conveyorB.on()
            if data.conveyorB_Reverse == True:
                self.reverse_conveyorB.on()
            else:
                self.reverse_conveyorB.off()
        else:
            self.run_conveyorB.off()

    def stopConveyor(self):
        self.run_conveyorB.off()
        self.run_conveyorF.off()
        self.reverse_conveyorB.off()
        self.reverse_conveyorF.off()

class Led:
    pass

class Speaker:
    def __init__(self):
        self.pin_speaker_on_or_off = 10 
        self.pin_speaker_mode = 22 
        self.set_pin_speaker_on_or_off = gpiozero.OutputDevice(self.pin_speaker_on_or_off)
        self.set_pin_speaker_mode = gpiozero.OutputDevice(self.pin_speaker_mode)
        rospy.Subscriber("speaker_topic", Speaker_msgs, self.speaker_callback)

    def speaker_callback(self,data):
        if data.run == True:
            self.set_pin_speaker_on_or_off.off()
            if data.mode == True:
                self.set_pin_speaker_mode.on()
            else:
                self.set_pin_speaker_mode.off()
            time.sleep(0.2)
            self.set_pin_speaker_on_or_off.on()
        else:
            self.set_pin_speaker_on_or_off.off()
#hai
class Safety:
    def __init__(self):
        self.port_sonar = rospy.get_param("~port_sonar", '/dev/ttyUSB1')
        self.sonar = serial.Serial(self.port_sonar, baudrate=9600, timeout=0.5)
        self.data_sonar = String()
        self.safety_pub = rospy.Publisher('safety_topic', String, queue_size=10)
        self.rate = rospy.get_param("~rate", 50)
        self.buff = []

    def openPort(self):
        """ Open port """
        if self.sonar.is_open == False:
            self.sonar.open()
        time.sleep(1)

    def closePort(self):
        """ Close port """
        self.sonar.reset_input_buffer()
        self.sonar.close()
        time.sleep(1)
    
    def read_sonar(self):
        """ Read data from sonar sensor """
        if self.sonar.is_open == True:
            read_data_sonar  = self.sonar.readline()
            if read_data_sonar:
                self.buff.append(read_data_sonar)
            if len(self.buff) == 4:
                str_val = self.buff[0].decode("utf-8") 
                str_val1 = self.buff[1].decode("utf-8")
                str_val2 = self.buff[2].decode("utf-8")
                str_val3 = self.buff[3].decode("utf-8")
                # print(str_val)
                # print(str_val1)
                # print(str_val2)
                # print(str_val3)
                if float(str_val1) < 50 or float(str_val2) < 50 or float(str_val3) < 50:  
                    self.data_sonar = "alram_sonar"
                    safety.safety_pub.publish(self.data_sonar)
                self.buff = []

        else:
            self.openPort()


if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_CONVEYOR')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    conveyor = Conveyor()
    speaker = Speaker()
    safety = Safety()
    conveyor.stopConveyor()
    rate = rospy.Rate(safety.rate)
    try:
        safety.openPort()
        while not rospy.is_shutdown():
            safety.read_sonar()
            rate.sleep()
    except rospy.ROSInterruptException:
        print("NODE AGV CONVEYOR ERROR")
    finally:
        conveyor.stopConveyor()