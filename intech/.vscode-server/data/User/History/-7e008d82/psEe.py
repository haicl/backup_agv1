#!/usr/bin/python3
from asyncore import read
from string import printable
from urllib.request import install_opener
import gpiozero
import roslib
import rospy
import time
import serial
import io
import codecs
from agv1_conveyor.msg import Conveyor_msgs
from agv1_gpio.msg import Led_msgs
from agv1_gpio.msg import Speaker_msgs
from std_msgs.msg import String

class Conveyor:
    def __init__(self):
        # Init hardware conveyor
        self.pin_Run_ConveyorF = 26
        self.pin_Reverse_ConveyorF = 19
        self.pin_Run_ConveyorB = 13
        self.pin_Reverse_ConveyorB = 6
        self.run_conveyorF = gpiozero.OutputDevice(self.pin_Run_ConveyorF)
        self.reverse_conveyorF = gpiozero.OutputDevice(self.pin_Reverse_ConveyorF)
        self.run_conveyorB = gpiozero.OutputDevice(self.pin_Run_ConveyorB)
        self.reverse_conveyorB = gpiozero.OutputDevice(self.pin_Reverse_ConveyorB)      
        
        # ROS Init
        rospy.Subscriber("conveyor_topic", Conveyor_msgs, self.conveyor_callback)
        self.rate = rospy.get_param("~rate", 10)

    def conveyor_callback(self,data):
        """ Recive messages from topic load_unload """
        # Conveyor forward
        if data.conveyorF_Run == True:
            self.run_conveyorF.on()
            if data.conveyorF_Reverse == True:
                self.reverse_conveyorF.off()
            else:
                self.reverse_conveyorF.on()
        else:
            self.run_conveyorF.off()

        # Conveyor backward
        if data.conveyorB_Run == True:
            self.run_conveyorB.on()
            if data.conveyorB_Reverse == True:
                self.reverse_conveyorB.on()
            else:
                self.reverse_conveyorB.off()
        else:
            self.run_conveyorB.off()

    def stopConveyor(self):
        self.run_conveyorB.off()
        self.run_conveyorF.off()
        self.reverse_conveyorB.off()
        self.reverse_conveyorF.off()

class Led:
    pass

class Speaker:
    def __init__(self):
        self.pin_speaker_on_or_off = 10 
        self.pin_speaker_mode = 22 
        self.set_pin_speaker_on_or_off = gpiozero.OutputDevice(self.pin_speaker_on_or_off)
        self.set_pin_speaker_mode = gpiozero.OutputDevice(self.pin_speaker_mode)
        rospy.Subscriber("speaker_topic", Speaker_msgs, self.speaker_callback)

    def speaker_callback(self,data):
        if data.run == True:
            self.set_pin_speaker_on_or_off.off()
            if data.mode == True:
                self.set_pin_speaker_mode.on()
            else:
                self.set_pin_speaker_mode.off()
            time.sleep(0.2)
            self.set_pin_speaker_on_or_off.on()
        else:
            self.set_pin_speaker_on_or_off.off()

#hai
class Safety:
    def __init__(self):
        self.pin_deceleration_agv = 0
        self.pin_stop_agv = 5
        self.deceleration_agv = gpiozero.InputDevice(self.pin_deceleration_agv)
        self.stop_agv = gpiozero.InputDevice(self.pin_stop_agv)
        self.safety_pub = rospy.Publisher('safety_topic', String, queue_size = 10)
        self.msg_safety_pub = String()

    def safety_process(self):
        if self.deceleration_agv.value == True:
            self.msg_safety_pub = "deceleration_agv"
            self.safety_pub.publish(self.msg_safety_pub)
    
        elif self.stop_agv.value == True:
            self.msg_safety_pub = "stop_agv"
            self.safety_pub.publish(self.msg_safety_pub)
        else:
            self.msg_safety_pub = "free_agv"
            self.safety_pub.publish(self.msg_safety_pub)


if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_CONVEYOR')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    conveyor = Conveyor()
    speaker = Speaker()
    safety = Safety()

    conveyor.stopConveyor()
    rate = rospy.Rate(conveyor.rate)
    try:
        while not rospy.is_shutdown():
            safety.safety_process()
            rate.sleep()
    except rospy.ROSInterruptException:
        print("NODE AGV CONVEYOR ERROR")
    finally:
        conveyor.stopConveyor()