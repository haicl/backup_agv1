#!/usr/bin/python3
import rospy
import roslib
import time
import serial
import io
from agv1_followline.msg import Followline_msgs
from std_msgs.msg import String


class Node_Followline():
    def __init__(self):
        # Hardware Followline
        self.totalNumber_eyes = 16
        self.numberEyesOn_flForward = 0
        self.numberEyesOn_flBackward = 0
        self.port_fl_forward = rospy.get_param("~port_fl_forward", '/dev/ttyUSB0')
        self.fl_forward = serial.Serial(self.port_fl_forward, baudrate=9600, timeout=0.5)
        # self.port_fl_backward = rospy.get_param("~port_fl_backward", '/dev/ttyUSB1')
        # self.fl_backward = serial.Serial(self.port_fl_backward , baudrate=9600, timeout=0.5)

        # ROS
        self.data_followLine = Followline_msgs()
        self.followline_pub = rospy.Publisher('followline_topic', Followline_msgs, queue_size=10)
        self.alarm_pub = rospy.Publisher('alarm_topic', String, queue_size=10)
        self.rate = rospy.get_param("~rate", 40)
        
    def openPort(self):
        """ Open port """
        if self.fl_forward.is_open == False:
            self.fl_forward.open()
        # if self.fl_backward.is_open == False:
        #     self.fl_backward.open()
        time.sleep(1)
    
    def closePort(self):
        """ Close port """
        self.fl_forward.reset_input_buffer()
        self.fl_forward.close()
        # self.fl_backward.reset_input_buffer()
        # self.fl_backward.close()
        time.sleep(1)

    def readFollowline_forward(self):
        """ Read dara from forward followline sensor """
        if self.fl_forward.is_open == True:
            data_forward = self.fl_forward.readline()
            if len(data_forward) > self.totalNumber_eyes:
                for i in range(self.totalNumber_eyes):
                    if data_forward[i] == 49:
                        self.data_followLine.forward[i] = 0
                    else:
                        self.data_followLine.forward[i] = 1
                        self.numberEyesOn_flForward += 1
        else:
            self.alarm_pub.publish("Error follow line forward")

    def readFollowline_backward(self):
        """ Read dara from backward followline sensor """
        if self.fl_backward.is_open == True:
            data_backward = self.fl_backward.readline()
            if len(data_backward) > self.totalNumber_eyes:
                for i in range(self.totalNumber_eyes):
                    if data_backward[i] == 49:
                        self.data_followLine.backward[i] = 0
                    else:
                        self.data_followLine.backward[i] = 1
        else:
            self.alarm_pub.publish("Error follow line backward")

    def calibDataFollowLine_forward(self):
        if self.numberEyesOn_flForward == 3:
            if self.data_followLine.forward[6:10] == [1,1,1,0] or self.data_followLine.forward[6:10] == [0,1,1,1]:
                self.data_followLine.forward[6] = 1
                self.data_followLine.forward[7] = 1
                self.data_followLine.forward[8] = 1
                self.data_followLine.forward[9] = 1
        else:
            pass
        self.numberEyesOn_flForward = 0
            
    #############################
    #           MAIN            #
    #############################
    def main(self):
        self.readFollowline_forward()
        # self.readFollowline_backward()

        self.calibDataFollowLine_forward()

        self.data_followLine.header.stamp = rospy.Time.now()
        self.data_followLine.header.frame_id = "followLine"
        self.followline_pub.publish(self.data_followLine)



if  __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_FOLLOWLINE')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    try:
        node_FL = Node_Followline()
        node_FL.openPort()
        rate = rospy.Rate(node_FL.rate)
        while not rospy.is_shutdown():
            node_FL.main()
            rate.sleep()
    except rospy.ROSInternalException:
        print("\n Error Node AGV1_FOLLOWLINE \n")
    finally: 
        node_FL.closePort()





