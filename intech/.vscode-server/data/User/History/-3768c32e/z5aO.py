#!/usr/bin/python3
import rospy
import roslib
import time
import serial
import io
from agv1_followline.msg import Followline_msgs
from std_msgs.msg import String


class Node_Followline():
    def __init__(self):
        # Hardware Followline
        self.totalNumber_eyes = 16
        self.number
        self.fl_forward = serial.Serial('/dev/ttyUSB0', baudrate=9600, timeout=0.5)
        # self.fl_backward = serial.Serial('/dev/ttyUSB1', baudrate=9600, timeout=0.5)

        # ROS
        self.data_followLine = Followline_msgs()
        self.followline_pub = rospy.Publisher('followline_topic', Followline_msgs, queue_size=10)
        self.alarm_pub = rospy.Publisher('alarm_topic', String, queue_size=10)

    def openPort(self):
        """ Open port """
        if self.fl_forward.is_open == False:
            self.fl_forward.open()
        # if self.fl_backward.is_open == False:
        #     self.fl_backward.open()
        time.sleep(1)
    
    def closePort(self):
        """ Close port """
        self.fl_forward.reset_input_buffer()
        self.fl_forward.close()
        # self.fl_backward.reset_input_buffer()
        # self.fl_backward.close()
        time.sleep(1)

    def readFollowline_forward(self):
        """ Read dara from forward followline sensor """
        if self.fl_forward.is_open == True:
            data_forward = self.fl_forward.readline()
            # print("f" + str(self.fl_forward.in_waiting))
            if len(data_forward) > self.totalNumber_eyes:
                for i in range(16):
                    if data_forward[i] == 49:
                        self.data_followLine.forward[i] = 0
                    else:
                        self.data_followLine.forward[i] = 1
        else:
            self.alarm_pub.publish("Error follow line forward")
    
    def readFollowline_backward(self):
        """ Read dara from backward followline sensor """
        if self.fl_backward.is_open == True:
            data_backward = self.fl_backward.readline()
            # print("b" + str(self.fl_backward.in_waiting))
            if len(data_backward) > 16:
                for i in range(16):
                    if data_backward[i] == 49:
                        self.data_followLine.backward[i] = 0
                    else:
                        self.data_followLine.backward[i] = 1
        else:
            self.alarm_pub.publish("Error follow line backward")

    def calibDataFollowLine_forward(self):
        for i in range(16):
            if self.data_followLine.forward[i] = 


    #############################
    #           MAIN            #
    #############################
    def main(self):
        self.readFollowline_forward()
        # self.readFollowline_backward()

        self.data_followLine.header.stamp = rospy.Time.now()
        self.data_followLine.header.frame_id = "followLine"
        self.followline_pub.publish(self.data_followLine)



if  __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_FOLLOWLINE')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    try:
        node_FL = Node_Followline()
        node_FL.openPort()
        rate = rospy.Rate(40)
        while not rospy.is_shutdown():
            node_FL.main()
            rate.sleep()
    except rospy.ROSInternalException:
        print("\n Error Node AGV1_FOLLOWLINE \n")
    finally: 
        node_FL.closePort()





