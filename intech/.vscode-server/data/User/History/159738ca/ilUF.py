#!/usr/bin/python3
import array as arr 
from itertools import count
import rospy
import serial
import time
from std_msgs.msg import String
from agv1_load_unload.msg import Conveyor_msgs

class NodeTest:
    def __init__(self):
        # Hardware init
        self.ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)
        
        # ROS Init
        self.pub = rospy.Publisher("GPIO_Conveyor", Conveyor_msgs,queue_size=1)
        self.number_subscriber = rospy.Subscriber("Load_UnLoad", String, self.callback)
        
        self.new_msg = Conveyor_msgs()
        self.state = False
        self.case = 0
        # Data
        self.data = ""
        self.count = 0
        self.buffData = []

        #setting 
        self.dir_conveyorF = True
        self.dir_conveyorB = True
        self.time_stop = 0.15

    def callback(self,data):
        str_recever = data.data.split("_",1)
        print(str_recever)

        if str_recever[0] == 'BT1L' and str_recever[1] == 'BT2L':
            self.new_msg.conveyorF_Run = True
            self.new_msg.conveyorF_Reverse = self.dir_conveyorF
            self.new_msg.conveyorB_Run = True
            self.new_msg.conveyorB_Reverse = self.dir_conveyorB

            self.pub.publish(self.new_msg)
            if self.ser.is_open == True:
                self.ser.write(b'BT1!')
            self.state = True

        elif str_recever[0] == 'BT1L' and str_recever[1] == 'BT2UL':
            self.new_msg.conveyorF_Run = True
            self.new_msg.conveyorF_Reverse = self.dir_conveyorF
            self.new_msg.conveyorB_Run = True
            self.new_msg.conveyorB_Reverse = not self.dir_conveyorB

            self.pub.publish(self.new_msg)

            if self.ser.is_open == True:
                self.ser.write(b'BT2!')
            self.state = True

        elif str_recever[0] == 'BT1UL' and str_recever[1] == 'BT2UL':
            self.new_msg.conveyorF_Run = True
            self.new_msg.conveyorF_Reverse = not self.dir_conveyorF
            self.new_msg.conveyorB_Run = True
            self.new_msg.conveyorB_Reverse = not self.dir_conveyorB

            self.pub.publish(self.new_msg)

            if self.ser.is_open == True:
                self.ser.write(b'BT3!')
            self.state = True      

        elif str_recever[0] == 'BT1UL' and str_recever[1] == 'BT2L':
            self.new_msg.conveyorF_Run = True
            self.new_msg.conveyorF_Reverse = not self.dir_conveyorF
            self.new_msg.conveyorB_Run = True
            self.new_msg.conveyorB_Reverse = self.dir_conveyorB

            self.pub.publish(self.new_msg)

            if self.ser.is_open == True:
                self.ser.write(b'BT4!')
            self.state = True

        elif str_recever[0] == 'BT1L' and str_recever[1] == 'BT2':
            self.new_msg.conveyorF_Run = True
            self.new_msg.conveyorF_Reverse = self.dir_conveyorF
            self.new_msg.conveyorB_Run = False

            self.pub.publish(self.new_msg)

            if self.ser.is_open == True:
                self.ser.write(b'BT5!') 
            self.state = True

        elif str_recever[0] == 'BT1UL' and str_recever[1] == 'BT2':
            self.new_msg.conveyorF_Run = True
            self.new_msg.conveyorF_Reverse = not self.dir_conveyorF
            self.new_msg.conveyorB_Run = False

            self.pub.publish(self.new_msg) 

            if self.ser.is_open == True:
                self.ser.write(b'BT6!')
            self.state = True

        elif str_recever[0] == 'BT1' and str_recever[1] == 'BT2UL':
            self.new_msg.conveyorF_Run = False
            self.new_msg.conveyorB_Run = True
            self.new_msg.conveyorB_Reverse = not self.dir_conveyorB

            self.pub.publish(self.new_msg)

            if self.ser.is_open == True:
                self.ser.write(b'BT7!') 
            self.state = True

        elif str_recever[0] == 'BT1' and str_recever[1] == 'BT2L':
            self.new_msg.conveyorF_Run = False
            self.new_msg.conveyorB_Run = True
            self.new_msg.conveyorB_Reverse = self.dir_conveyorB

            self.pub.publish(self.new_msg)

            if self.ser.is_open == True:
                self.ser.write(b'BT8!')
            self.state = True

        else:
            self.state = False

    def openPort(self):
        """ Open port """
        if self.ser.is_open == False:
            self.ser.open()
        time.sleep(1)

    def closePort(self):
        """ Close port """
        self.ser.reset_input_buffer()
        self.ser.close()
        time.sleep(1)

    def readData(self):
        if self.ser.is_open == True:
            if self.ser.in_waiting > 0:
                self.data = self.ser.read(1)
                # print(self.data)
                if self.data == b'!' and len(self.buffData)>=5:
                    if self.buffData[len(self.buffData)-1] == b'K' and self.buffData[len(self.buffData)-2] == b'O': 
                        if  self.buffData[len(self.buffData)-4] == b'T' and self.buffData[len(self.buffData)-5] == b'B':
                            self.case = (self.buffData[len(self.buffData)-3])
                            self.compare()
                            self.buffData.clear()
                            self.count = 0
                else:
                    self.buffData.append(self.data)
                    self.count += 1
        else:
            self.ser.open()

    def compare(self):
        if self.case == b'1' and self.state == True:
            self.new_msg.conveyorB_Run = False
        elif self.case == b'2' and self.state == True:
            self.new_msg.conveyorF_Run = False
        elif self.case == b'3' and self.state == True:
            time.sleep(self.time_stop)
            self.new_msg.conveyorB_Run = False
        elif self.case == b'4' and self.state == True:
            self.new_msg.conveyorF_Run = False
        elif self.case == b'5' and self.state == True:
            time.sleep(self.time_stop)
            self.new_msg.conveyorB_Run = False
        elif self.case == b'6' and self.state == True:
            time.sleep(self.time_stop)
            self.new_msg.conveyorF_Run = False
        elif self.case == b'7' and self.state == True:
            self.new_msg.conveyorB_Run = False
        elif self.case == b'8' and self.state == True:
            time.sleep(self.time_stop)
            self.new_msg.conveyorF_Run = False
        elif self.case == b'9' and self.state == True:
            self.new_msg.conveyorF_Run = False
            time.sleep(self.time_stop)
        elif self.case == b'0' and self.state == True:
            time.sleep(self.time_stop)
            self.new_msg.conveyorB_Run = False                    
        else:
            pass

        self.pub.publish(self.new_msg) 


if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_LOAD_UNLOAD')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    try:
        node_load_unload = NodeTest()
        rate = rospy.Rate(20)
        while not rospy.is_shutdown():
            node_load_unload.readData()
            rate.sleep()
    except rospy.ROSInterruptException:
        print("NODE AGV LOAD_UNLOAD ERROR")
    finally:
        node_load_unload.closePort()