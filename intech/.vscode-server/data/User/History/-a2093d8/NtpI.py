#!/usr/bin/env python
import rospy
import roslib
import time
import serial
from agv1_rfid.msg import CardRFID_msgs


class Node_Read_RFID():
    def __init__(self):
        # Data
        self.newCard = 0
        self.oldCard = 0
        self.data = ""
        self.data1 = ""
        self.buffData = ""
        self.readRFID = serial.Serial('/dev/ttyUSB0', baudrate=9600, timeout= 0.5)

        #ROS
        self.rate = 10
        self.cardRFID_pub = rospy.Publisher('rfid_topic', CardRFID_msgs, queue_size=10)
        self.new_msgs = CardRFID_msgs()

    def openPort(self):
        """ Open port """
        if self.readRFID.is_open == False:
            self.readRFID.open()
            time.sleep(1)

    def closePort(self):
        self.readRFID.reset_input_buffer()
        self.readRFID.close()
        time.sleep(1)

    def readDataRFID(self):
        """ Read data from module RFID """
        if self.readRFID.is_open == True:
            if  self.readRFID.in_waiting >= 40:
                self.buffData = self.readRFID.read(self.readRFID.in_waiting)
                self.data = self.buffData[:20]
                self.data1 = self.buffData[20:40]
                # print(self.data)
                # print(self.data1)
                if self.data == self.data1:
                    self.new_msgs.status = "success"
                    self.new_msgs.old_cardRFID = self.new_msgs.new_cardRFID
                    self.new_msgs.new_cardRFID = int(self.data[15:17])
                else:
                    self.new_msgs.status = "error"

                self.cardRFID_pub.publish(self.new_msgs)
        else:
            self.readRFID.open()

    #############################
    #           MAIN            #
    #############################
    def main(self):
        self.readDataRFID()
        #self.cardRFID_pub.publish(self.newCard)

if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_READ_RFID')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    agv1_readRFID = Node_Read_RFID()
    try:
        rate = rospy.Rate(agv1_readRFID.rate) 
        while not rospy.is_shutdown():
            agv1_readRFID.main()
            rate.sleep()

    except rospy.ROSInternalException:
        print("\n Node Master ERROR \n")
    finally:
        # agv1_readRFID.stopAGV()
        rospy.signal_shutdown("Shutdown node AGV1_READ_RFID")