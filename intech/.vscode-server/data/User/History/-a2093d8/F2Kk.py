#!/usr/bin/env python
import rospy
import roslib
import time
import serial
from agv1_rfid.msg import CardRFID_msgs


class Node_Read_RFID():
    def __init__(self):
        # Data
        self.newCard = 0
        self.oldCard = 0
        self.buffData = ""
        self.readRFID = serial.Serial('/dev/ttyUSB0', baudrate=9600, timeout= 0.5)

        #ROS
        self.rate = 5
        self.cardRFID_pub = rospy.Publisher('rfid_topic', CardRFID_msgs, queue_size=10)
        self.new_msgs = CardRFID_msgs()
        self.new_msgs.header.frame_id = "rfid"

    def openPort(self):
        """ Open port """
        if self.readRFID.is_open == False:
            self.readRFID.open()
            time.sleep(1)

    def closePort(self):
        self.readRFID.reset_input_buffer()
        self.readRFID.close()
        time.sleep(1)

    def readDataRFID(self):
        """ Read data from module RFID """
        if self.readRFID.is_open == True:
            if  self.readRFID.in_waiting >= 40:
                self.buffData = self.readRFID.read(self.readRFID.in_waiting)
                if self.buffData[0:20] == self.buffData[20:40]:
                    self.new_msgs.status = "success"
                    self.new_msgs.header.stamp = rospy.Time.now()
                    self.new_msgs.old_cardRFID = self.new_msgs.new_cardRFID
                    self.new_msgs.new_cardRFID = int(self.buffData[15:17])
                else:
                    self.new_msgs.status = "error"
                    self.new_msgs.header.stamp = rospy.Time.now()
                    self.new_msgs.new_cardRFID = 0

                self.cardRFID_pub.publish(self.new_msgs)
        else:
            self.readRFID.open()

    #############################
    #           MAIN            #
    #############################
    def main(self):
        self.readDataRFID()

if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_READ_RFID')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    agv1_readRFID = Node_Read_RFID()
    try:
        rate = rospy.Rate(agv1_readRFID.rate) 
        while not rospy.is_shutdown():
            agv1_readRFID.main()
            rate.sleep()

    except rospy.ROSInternalException:
        print("\n Node Master ERROR \n")
    finally:
        agv1_readRFID.closePort()