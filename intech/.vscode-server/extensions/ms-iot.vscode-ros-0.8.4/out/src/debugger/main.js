"use strict";
// Copyright (c) Andrew Short. All rights reserved.
// Licensed under the MIT License.
Object.defineProperty(exports, "__esModule", { value: true });
const session = require("./debug-session");
session.RosDebugSession.run(session.RosDebugSession);
//# sourceMappingURL=main.js.map