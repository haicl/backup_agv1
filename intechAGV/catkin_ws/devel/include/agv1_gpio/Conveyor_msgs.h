// Generated by gencpp from file agv1_gpio/Conveyor_msgs.msg
// DO NOT EDIT!


#ifndef AGV1_GPIO_MESSAGE_CONVEYOR_MSGS_H
#define AGV1_GPIO_MESSAGE_CONVEYOR_MSGS_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace agv1_gpio
{
template <class ContainerAllocator>
struct Conveyor_msgs_
{
  typedef Conveyor_msgs_<ContainerAllocator> Type;

  Conveyor_msgs_()
    : conveyorF_Run(false)
    , conveyorF_Reverse(false)
    , conveyorB_Run(false)
    , conveyorB_Reverse(false)  {
    }
  Conveyor_msgs_(const ContainerAllocator& _alloc)
    : conveyorF_Run(false)
    , conveyorF_Reverse(false)
    , conveyorB_Run(false)
    , conveyorB_Reverse(false)  {
  (void)_alloc;
    }



   typedef uint8_t _conveyorF_Run_type;
  _conveyorF_Run_type conveyorF_Run;

   typedef uint8_t _conveyorF_Reverse_type;
  _conveyorF_Reverse_type conveyorF_Reverse;

   typedef uint8_t _conveyorB_Run_type;
  _conveyorB_Run_type conveyorB_Run;

   typedef uint8_t _conveyorB_Reverse_type;
  _conveyorB_Reverse_type conveyorB_Reverse;





  typedef boost::shared_ptr< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> const> ConstPtr;

}; // struct Conveyor_msgs_

typedef ::agv1_gpio::Conveyor_msgs_<std::allocator<void> > Conveyor_msgs;

typedef boost::shared_ptr< ::agv1_gpio::Conveyor_msgs > Conveyor_msgsPtr;
typedef boost::shared_ptr< ::agv1_gpio::Conveyor_msgs const> Conveyor_msgsConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator1> & lhs, const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator2> & rhs)
{
  return lhs.conveyorF_Run == rhs.conveyorF_Run &&
    lhs.conveyorF_Reverse == rhs.conveyorF_Reverse &&
    lhs.conveyorB_Run == rhs.conveyorB_Run &&
    lhs.conveyorB_Reverse == rhs.conveyorB_Reverse;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator1> & lhs, const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace agv1_gpio

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
{
  static const char* value()
  {
    return "111548c16238592512d635263bd304c3";
  }

  static const char* value(const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x111548c162385925ULL;
  static const uint64_t static_value2 = 0x12d635263bd304c3ULL;
};

template<class ContainerAllocator>
struct DataType< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
{
  static const char* value()
  {
    return "agv1_gpio/Conveyor_msgs";
  }

  static const char* value(const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool conveyorF_Run\n"
"bool conveyorF_Reverse\n"
"bool conveyorB_Run\n"
"bool conveyorB_Reverse\n"
;
  }

  static const char* value(const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.conveyorF_Run);
      stream.next(m.conveyorF_Reverse);
      stream.next(m.conveyorB_Run);
      stream.next(m.conveyorB_Reverse);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Conveyor_msgs_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::agv1_gpio::Conveyor_msgs_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::agv1_gpio::Conveyor_msgs_<ContainerAllocator>& v)
  {
    s << indent << "conveyorF_Run: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.conveyorF_Run);
    s << indent << "conveyorF_Reverse: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.conveyorF_Reverse);
    s << indent << "conveyorB_Run: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.conveyorB_Run);
    s << indent << "conveyorB_Reverse: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.conveyorB_Reverse);
  }
};

} // namespace message_operations
} // namespace ros

#endif // AGV1_GPIO_MESSAGE_CONVEYOR_MSGS_H
