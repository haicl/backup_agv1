; Auto-generated. Do not edit!


(cl:in-package agv1_conveyor-msg)


;//! \htmlinclude Conveyor_msgs.msg.html

(cl:defclass <Conveyor_msgs> (roslisp-msg-protocol:ros-message)
  ((conveyorF_Run
    :reader conveyorF_Run
    :initarg :conveyorF_Run
    :type cl:boolean
    :initform cl:nil)
   (conveyorF_Reverse
    :reader conveyorF_Reverse
    :initarg :conveyorF_Reverse
    :type cl:boolean
    :initform cl:nil)
   (conveyorB_Run
    :reader conveyorB_Run
    :initarg :conveyorB_Run
    :type cl:boolean
    :initform cl:nil)
   (conveyorB_Reverse
    :reader conveyorB_Reverse
    :initarg :conveyorB_Reverse
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Conveyor_msgs (<Conveyor_msgs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Conveyor_msgs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Conveyor_msgs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name agv1_conveyor-msg:<Conveyor_msgs> is deprecated: use agv1_conveyor-msg:Conveyor_msgs instead.")))

(cl:ensure-generic-function 'conveyorF_Run-val :lambda-list '(m))
(cl:defmethod conveyorF_Run-val ((m <Conveyor_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv1_conveyor-msg:conveyorF_Run-val is deprecated.  Use agv1_conveyor-msg:conveyorF_Run instead.")
  (conveyorF_Run m))

(cl:ensure-generic-function 'conveyorF_Reverse-val :lambda-list '(m))
(cl:defmethod conveyorF_Reverse-val ((m <Conveyor_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv1_conveyor-msg:conveyorF_Reverse-val is deprecated.  Use agv1_conveyor-msg:conveyorF_Reverse instead.")
  (conveyorF_Reverse m))

(cl:ensure-generic-function 'conveyorB_Run-val :lambda-list '(m))
(cl:defmethod conveyorB_Run-val ((m <Conveyor_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv1_conveyor-msg:conveyorB_Run-val is deprecated.  Use agv1_conveyor-msg:conveyorB_Run instead.")
  (conveyorB_Run m))

(cl:ensure-generic-function 'conveyorB_Reverse-val :lambda-list '(m))
(cl:defmethod conveyorB_Reverse-val ((m <Conveyor_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv1_conveyor-msg:conveyorB_Reverse-val is deprecated.  Use agv1_conveyor-msg:conveyorB_Reverse instead.")
  (conveyorB_Reverse m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Conveyor_msgs>) ostream)
  "Serializes a message object of type '<Conveyor_msgs>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'conveyorF_Run) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'conveyorF_Reverse) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'conveyorB_Run) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'conveyorB_Reverse) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Conveyor_msgs>) istream)
  "Deserializes a message object of type '<Conveyor_msgs>"
    (cl:setf (cl:slot-value msg 'conveyorF_Run) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'conveyorF_Reverse) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'conveyorB_Run) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'conveyorB_Reverse) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Conveyor_msgs>)))
  "Returns string type for a message object of type '<Conveyor_msgs>"
  "agv1_conveyor/Conveyor_msgs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Conveyor_msgs)))
  "Returns string type for a message object of type 'Conveyor_msgs"
  "agv1_conveyor/Conveyor_msgs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Conveyor_msgs>)))
  "Returns md5sum for a message object of type '<Conveyor_msgs>"
  "111548c16238592512d635263bd304c3")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Conveyor_msgs)))
  "Returns md5sum for a message object of type 'Conveyor_msgs"
  "111548c16238592512d635263bd304c3")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Conveyor_msgs>)))
  "Returns full string definition for message of type '<Conveyor_msgs>"
  (cl:format cl:nil "bool conveyorF_Run~%bool conveyorF_Reverse~%bool conveyorB_Run~%bool conveyorB_Reverse~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Conveyor_msgs)))
  "Returns full string definition for message of type 'Conveyor_msgs"
  (cl:format cl:nil "bool conveyorF_Run~%bool conveyorF_Reverse~%bool conveyorB_Run~%bool conveyorB_Reverse~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Conveyor_msgs>))
  (cl:+ 0
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Conveyor_msgs>))
  "Converts a ROS message object to a list"
  (cl:list 'Conveyor_msgs
    (cl:cons ':conveyorF_Run (conveyorF_Run msg))
    (cl:cons ':conveyorF_Reverse (conveyorF_Reverse msg))
    (cl:cons ':conveyorB_Run (conveyorB_Run msg))
    (cl:cons ':conveyorB_Reverse (conveyorB_Reverse msg))
))
