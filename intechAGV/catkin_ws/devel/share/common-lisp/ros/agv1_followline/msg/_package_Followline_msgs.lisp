(cl:in-package agv1_followline-msg)
(cl:export '(HEADER-VAL
          HEADER
          FORWARD-VAL
          FORWARD
          BACKWARD-VAL
          BACKWARD
))