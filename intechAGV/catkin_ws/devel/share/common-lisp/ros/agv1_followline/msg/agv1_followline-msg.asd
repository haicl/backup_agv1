
(cl:in-package :asdf)

(defsystem "agv1_followline-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Followline_msgs" :depends-on ("_package_Followline_msgs"))
    (:file "_package_Followline_msgs" :depends-on ("_package"))
  ))