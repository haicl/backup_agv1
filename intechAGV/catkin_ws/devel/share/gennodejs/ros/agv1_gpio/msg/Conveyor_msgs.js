// Auto-generated. Do not edit!

// (in-package agv1_gpio.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Conveyor_msgs {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.conveyorF_Run = null;
      this.conveyorF_Reverse = null;
      this.conveyorB_Run = null;
      this.conveyorB_Reverse = null;
    }
    else {
      if (initObj.hasOwnProperty('conveyorF_Run')) {
        this.conveyorF_Run = initObj.conveyorF_Run
      }
      else {
        this.conveyorF_Run = false;
      }
      if (initObj.hasOwnProperty('conveyorF_Reverse')) {
        this.conveyorF_Reverse = initObj.conveyorF_Reverse
      }
      else {
        this.conveyorF_Reverse = false;
      }
      if (initObj.hasOwnProperty('conveyorB_Run')) {
        this.conveyorB_Run = initObj.conveyorB_Run
      }
      else {
        this.conveyorB_Run = false;
      }
      if (initObj.hasOwnProperty('conveyorB_Reverse')) {
        this.conveyorB_Reverse = initObj.conveyorB_Reverse
      }
      else {
        this.conveyorB_Reverse = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Conveyor_msgs
    // Serialize message field [conveyorF_Run]
    bufferOffset = _serializer.bool(obj.conveyorF_Run, buffer, bufferOffset);
    // Serialize message field [conveyorF_Reverse]
    bufferOffset = _serializer.bool(obj.conveyorF_Reverse, buffer, bufferOffset);
    // Serialize message field [conveyorB_Run]
    bufferOffset = _serializer.bool(obj.conveyorB_Run, buffer, bufferOffset);
    // Serialize message field [conveyorB_Reverse]
    bufferOffset = _serializer.bool(obj.conveyorB_Reverse, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Conveyor_msgs
    let len;
    let data = new Conveyor_msgs(null);
    // Deserialize message field [conveyorF_Run]
    data.conveyorF_Run = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [conveyorF_Reverse]
    data.conveyorF_Reverse = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [conveyorB_Run]
    data.conveyorB_Run = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [conveyorB_Reverse]
    data.conveyorB_Reverse = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'agv1_gpio/Conveyor_msgs';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '111548c16238592512d635263bd304c3';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool conveyorF_Run
    bool conveyorF_Reverse
    bool conveyorB_Run
    bool conveyorB_Reverse
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Conveyor_msgs(null);
    if (msg.conveyorF_Run !== undefined) {
      resolved.conveyorF_Run = msg.conveyorF_Run;
    }
    else {
      resolved.conveyorF_Run = false
    }

    if (msg.conveyorF_Reverse !== undefined) {
      resolved.conveyorF_Reverse = msg.conveyorF_Reverse;
    }
    else {
      resolved.conveyorF_Reverse = false
    }

    if (msg.conveyorB_Run !== undefined) {
      resolved.conveyorB_Run = msg.conveyorB_Run;
    }
    else {
      resolved.conveyorB_Run = false
    }

    if (msg.conveyorB_Reverse !== undefined) {
      resolved.conveyorB_Reverse = msg.conveyorB_Reverse;
    }
    else {
      resolved.conveyorB_Reverse = false
    }

    return resolved;
    }
};

module.exports = Conveyor_msgs;
