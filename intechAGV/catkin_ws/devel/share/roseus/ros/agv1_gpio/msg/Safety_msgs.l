;; Auto-generated. Do not edit!


(when (boundp 'agv1_gpio::Safety_msgs)
  (if (not (find-package "AGV1_GPIO"))
    (make-package "AGV1_GPIO"))
  (shadow 'Safety_msgs (find-package "AGV1_GPIO")))
(unless (find-package "AGV1_GPIO::SAFETY_MSGS")
  (make-package "AGV1_GPIO::SAFETY_MSGS"))

(in-package "ROS")
;;//! \htmlinclude Safety_msgs.msg.html


(defclass agv1_gpio::Safety_msgs
  :super ros::object
  :slots (_sonar_cm ))

(defmethod agv1_gpio::Safety_msgs
  (:init
   (&key
    ((:sonar_cm __sonar_cm) (make-array 4 :initial-element 0 :element-type :integer))
    )
   (send-super :init)
   (setq _sonar_cm __sonar_cm)
   self)
  (:sonar_cm
   (&optional __sonar_cm)
   (if __sonar_cm (setq _sonar_cm __sonar_cm)) _sonar_cm)
  (:serialization-length
   ()
   (+
    ;; int8[4] _sonar_cm
    (* 1    4)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8[4] _sonar_cm
     (dotimes (i 4)
       (write-byte (elt _sonar_cm i) s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8[4] _sonar_cm
   (dotimes (i (length _sonar_cm))
     (setf (elt _sonar_cm i) (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> (elt _sonar_cm i) 127) (setf (elt _sonar_cm i) (- (elt _sonar_cm i) 256)))
     )
   ;;
   self)
  )

(setf (get agv1_gpio::Safety_msgs :md5sum-) "12f7c06a1516e3ef5402d7242919303c")
(setf (get agv1_gpio::Safety_msgs :datatype-) "agv1_gpio/Safety_msgs")
(setf (get agv1_gpio::Safety_msgs :definition-)
      "int8[4] sonar_cm
")



(provide :agv1_gpio/Safety_msgs "12f7c06a1516e3ef5402d7242919303c")


