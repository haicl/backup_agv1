#!/usr/bin/python3
import smbus
import rospy
from time import sleep 
from std_msgs.msg import String


def test_I2c():
    value = 1500

    i2cBus = smbus.SMBus(1)
    address = 0x60
    reg_write_dac = 0x40

    pub = rospy.Publisher('test_I2c', String, queue_size=10)
    rospy.init_node('test_I2c', anonymous=True)
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        str_printer = "Test I2c: " + "(add: " + str(address) + " - value: " + str(value) + ")"
        rospy.loginfo(str_printer)
        pub.publish(str_printer)

        msg = (value & 0xff0) >> 4
        msg = [msg, (msg & 0xf) << 4]
        # Write out I2C command: address, reg_write_dac, msg[0], msg[1]
        i2cBus.write_i2c_block_data(address, reg_write_dac, msg)

        rate.sleep()

if __name__ == '__main__':
    """ main """
    try:
        test_I2c()
    except rospy.ROSInterruptException:
        pass
