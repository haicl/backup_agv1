import time
import board
import neopixel
from neopixel_write import neopixel_write
import adafruit_pixelbuf


# Loop forever and blink the color
while True:
   LEDcount = 10
   pixels = neopixel.NeoPixel(board.D18, LEDcount)
   pixels.fill((100,0,0))
   sleep(1)
