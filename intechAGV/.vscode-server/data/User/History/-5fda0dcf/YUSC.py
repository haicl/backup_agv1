import time
import board
import neopixel
from neopixel_write import neopixel_write
import adafruit_pixelbuf

while True:
   pixel_pin = board.D18
   num_pixels = 10
   ORDER = neopixel.GRB

   pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness = 0.2, auto_write = False, pixel_order = ORDER)
   pixels.fill((255, 0, 0))
   # pixels.show()
   time.sleep(1)