import time
import board
import neopixel
from neopixel_write import neopixel_write
# import adafruit_pixelbuf

# Configure the setup
PIXEL_PIN = board.D18  # pin that the NeoPixel is connected to
ORDER = neopixel.RGB  # pixel color channel order
COLOR = (100, 50, 150)  # color to blink
CLEAR = (0, 0, 0)  # clear (or second color)
DELAY = 0.25  # blink rate in seconds

# Create the NeoPixel object
pixel = neopixel.NeoPixel(PIXEL_PIN, 1, pixel_order=ORDER)

# Loop forever and blink the color
while True:
   pixel[0] = COLOR
   time.sleep(DELAY)
   pixel[0] = CLEAR
   time.sleep(DELAY)