import time
import board
import neopixel
from neopixel_write import neopixel_write
import adafruit_pixelbuf

def rainbow_cycle(wait):
   for j in range(255):
      for i in range(num_pixels):
         pixel_index = (i * 256 // num_pixels) + j
         pixels[i] = wheel(pixel_index & 255)
      pixels.show()
      time.sleep(wait)

while True:
   pixel_pin = board.D18
   num_pixels = 10
   ORDER = neopixel.GRB

   pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness = 0.2, auto_write = False, pixel_order = ORDER)
   pixels.fill((255, 0, 0))
   # pixels.show()
   time.sleep(1)
   pixels.fill((0, 255, 0))
   time.sleep(1)
   pixels.fill((0, 0, 255))
   time.sleep(1)

   rainbow_cycle(0.001)