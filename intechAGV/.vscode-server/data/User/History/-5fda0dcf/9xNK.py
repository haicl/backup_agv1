import time
import board
import neopixel
from neopixel_write import neopixel_write
import adafruit_pixelbuf

try:
    # Used only for typing
    from typing import Optional, Type
    from types import TracebackType
    import microcontroller
except ImportError:
    pass

# Choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D18
# NeoPixels must be connected to D10, D12, D18 or D21 to work.
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 30

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)
while True:
   pixels.fill((255, 0, 0))
   # pixels.show()
   time.sleep(1)
   # print("ok")