#!/usr/bin/python3
import rospy
import roslib
import time
import serial
from agv1_rfid.msg import CardRFID_msgs


class Node_Read_RFID():
    
    def __init__(self):
        # Data
        self.newCard = 0
        self.oldCard = 0
        self.buffData = ""
        self.portRFID = rospy.get_param("~port_RFID", '/dev/ttyUSB_RFID')
        self.readRFID = serial.Serial(self.portRFID, baudrate=9600, timeout= 0.5)

        #ROS
        self.cardRFID_pub = rospy.Publisher('rfid_topic', CardRFID_msgs, queue_size=10)
        self.cardRFID_msgs = CardRFID_msgs()
        self.cardRFID_msgs.header.frame_id = "rfid"
        self.rate = rospy.get_param("~rate", 5)

    def openPort(self):
        """ Open port """
        if self.readRFID.is_open == False:
            self.readRFID.open()
            time.sleep(1)

    def closePort(self):
        """ Close port """
        self.readRFID.reset_input_buffer()
        self.readRFID.close()
        time.sleep(1)

    def readDataRFID(self):
        """ Read data from module RFID """
        if self.readRFID.is_open == True:
            if  self.readRFID.in_waiting >= 40:
                self.buffData = self.readRFID.read(self.readRFID.in_waiting)
                if self.buffData[0:20] == self.buffData[20:40]:
                    self.cardRFID_msgs.status = "success"
                    self.cardRFID_msgs.header.stamp = rospy.Time.now()
                    self.cardRFID_msgs.old_cardRFID = self.cardRFID_msgs.new_cardRFID
                    self.cardRFID_msgs.new_cardRFID = int(self.buffData[16:18])
                else:
                    self.cardRFID_msgs.status = "error"
                    self.cardRFID_msgs.header.stamp = rospy.Time.now()

                self.cardRFID_pub.publish(self.cardRFID_msgs)
        else:
            self.readRFID.open()

    #############################
    #           MAIN            #
    #############################
    def main(self):
        self.readDataRFID()

if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_READ_RFID')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    agv1_readRFID = Node_Read_RFID()
    try:
        rate = rospy.Rate(agv1_readRFID.rate) 
        while not rospy.is_shutdown():
            agv1_readRFID.main()
            rate.sleep()

    except rospy.ROSInternalException:
        print("\n Node Read RFID ERROR \n")
    finally:
        agv1_readRFID.closePort()