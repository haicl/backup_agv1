#!/usr/bin/python3
import rospy
import roslib
import time
import serial
from pymodbus.client import ModbusSerialClient as ModbusClient
import time
from enum import Enum, unique
from std_msgs.msg import String
from agv1_conveyor.msg import Conveyor_msgs
from agv1_xbee.msg import XbeeLoadUnload_msgs
import json

# @unique
class Node_Xbee:
    def __init__(self):
        #Hardware init
        self.xbee = ModbusClient(method ='~port_xbee' ,port = '/dev/ttyUSB_XBEE', stopbits = 1, bytesize = 8, parity = 'N', baudrate = 9600, timeout=0.5) 

        #Init ROS
        self.rate = rospy.get_param("~rate", 50)
        rospy.Subscriber('xbeeConveyor_topic', XbeeLoadUnload_msgs, self.xbeeConveryor_callback)
        
        self.xbeeConveyor_pub = rospy.Publisher('xbeeConveyor_topic', XbeeLoadUnload_msgs, queue_size = 10)
        self.msg_xbeeConveyor = XbeeLoadUnload_msgs()
        self.conveyor_pub = rospy.Publisher('conveyor_topic',Conveyor_msgs, queue_size=10)
        self.msg_conveyor = Conveyor_msgs()

        #Setting 
        self.dir_conveyorF = False
        self.dir_conveyorB = False

        self.flag_finish = 0
        self.flag_stopConveyorF = False
        self.flag_stopConveyorB = False
        self.countF = 0
        self.countB = 0
        self.time_stopConveyorF = 0   #dv: s
        self.time_stopConveyorB = 0   #dv: s
        self.timeExport = 0.4 # 0.4

        self.flag_readatashort = False
        self.flag_readatalong = False
        self.flag_check = False

        self.addres =0
        self.id = 0

        self.test = 0

    def openPort(self):
        """ Open port """
        if self.xbee.connect() == False:
            self.xbee.connect()
        time.sleep(1)

    def closePort(self):
        """ Close port """
        self.xbee.close()
        time.sleep(1)

    def xbeeConveryor_callback(self,data):
        """ Receve messages from topic xbeeConveyor_topic """
        if data.status == "busy" and data.enable == True:
            if data.station_selection == "shortConveyor":
                self.flag_readatashort = True
                time.sleep(1)
                self.run_shortConveyor(data.case)
            elif data.station_selection == "longConveyor":
                self.flag_readatalong = True
                time.sleep(1)
                self.run_longCoveyor(data.case)
            else:
                print("error message from topic xbeeConveyor")
        elif data.status == "finish" and data.enable == False:
            self.flag_busy = False
        else:
            print("error message from topic xbeeConveyor")

    def run_shortConveyor(self,case):
        """ Run short Conveyor """
        if case == 1: # 2 in
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF  
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(0,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)
            self.flag_check = True
            self.addres = 0
            self.id = 1
            self.time_stopConveyorF = 0   #dv: s
            self.time_stopConveyorB = 0   #dv: s
            print("input short")

        elif case == 2: # F out, B in
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF 
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB #
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect()== True:
                self.xbee.write_coil(2,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 3: # 2 out
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(1,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)

            self.flag_check = True
            self.addres = 1
            self.id = 1

            self.time_stopConveyorF = 0   #dv: s
            self.time_stopConveyorB = 0.025   #dv: s
            print("output short")
        elif case == 4: # F in , B out
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(3,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 5: # B in, F stop
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB #
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(4,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 6: # B out, F stop
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB #
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(5,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 7: # F in, B stop
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.connect() == True:
                self.xbee.write_coil(6,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 8: # F out, B stop
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.connect() == True:
                self.xbee.write_coil(7,1,1)
                self.conveyor_pub.publish(self.msg_conveyor)
        else:
            print("error message from topic xbeeConveyor")

    def run_longCoveyor(self,case):
        """ Run long Conveyor """
        if case == 1: #2 in
            self.msg_conveyor.conveyorF_Reverse = self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(0,1,2)
                self.conveyor_pub.publish(self.msg_conveyor)  
            self.flag_check = True
            self.addres = 0
            self.id = 2             
            self.time_stopConveyorF = 0   #dv: s
            self.time_stopConveyorB = 0   #dv: s
            print("input long")

        elif case == 2: #F out, B in
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse =  self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(2,1,2)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 3: #2 out
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(1,1,2)
                self.conveyor_pub.publish(self.msg_conveyor)
            self.flag_check = True
            self.addres = 1
            self.id = 2
            self.time_stopConveyorF = 0  #dv: s
            self.time_stopConveyorB = 0  #dv: s
            print("output long")

        elif case == 4: # F in , B out
            self.msg_conveyor.conveyorF_Reverse =  self.dir_conveyorF
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(3,1,2)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 5: # B in
            self.msg_conveyor.conveyorB_Reverse = self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(4,1,2)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 6: #B out
            self.msg_conveyor.conveyorB_Reverse = not self.dir_conveyorB
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = True
            if self.xbee.connect() == True:
                self.xbee.write_coil(5,1,2)
                self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 7:# F out
            self.msg_conveyor.conveyorF_Reverse = not self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.connect() == True:
               self.xbee.write_coil(7,1,2)
               self.conveyor_pub.publish(self.msg_conveyor)
        elif case == 8: # F in
            self.msg_conveyor.conveyorF_Reverse =  self.dir_conveyorF
            self.msg_conveyor.conveyorF_Run = True
            self.msg_conveyor.conveyorB_Run = False
            if self.xbee.connect() == True:
                self.xbee.write_coil(6,1,2)
                self.conveyor_pub.publish(self.msg_conveyor)
        else:
            print("error message from topic xbeeConveyor")

    def stopConveyor(self):
        """ Stop short Conveyor """
        if self.flag_stopConveyorF == True and self.msg_conveyor.conveyorF_Run == True:
            self.countF += 1
            if self.countF >= (self.time_stopConveyorF * self.rate):
                self.flag_stopConveyorF = False
                self.countF = 0
                self.flag_finish += 1
                self.msg_conveyor.conveyorF_Run = False
                self.conveyor_pub.publish(self.msg_conveyor)
                self.time_stopConveyorF = 0
        
        if self.flag_stopConveyorB == True and self.msg_conveyor.conveyorB_Run == True:
            self.countB += 1
            if self.countB >= (self.time_stopConveyorB * self.rate):
                self.flag_stopConveyorB = False
                self.countB = 0
                self.flag_finish += 1
                self.msg_conveyor.conveyorB_Run = False
                self.conveyor_pub.publish(self.msg_conveyor)   
                self.time_stopConveyorB = 0

    def readData(self):
        """ Read data from xbee """
        if self.xbee.connect() == True:
            if(self.flag_readatashort == True):
                try:
                    time.sleep(1)
                    result =  self.xbee.read_coils(8, 2, 1) 
                    if(result.bits[0] == 1):
                        self.flag_stopConveyorB = True
                        self.xbee.write_coil(8,0,1)
                        self.conveyor_pub.publish(self.msg_conveyor)
                    if(result.bits[1] == 1):
                        self.flag_stopConveyorF = True
                        self.xbee.write_coil(9,0,1)
                        self.conveyor_pub.publish(self.msg_conveyor)
                except:
                    pass
        else:
            self.openPort()  

    def readDatalong(self):
        """ Read data from xbee """
        if self.xbee.connect() == True:
            if(self.flag_readatalong == True):
                try:
                    time.sleep(1)
                    result =  self.xbee.read_coils(8, 2, 2) 
                    if(result.bits[0] == 1):
                        self.flag_stopConveyorB = True
                        self.xbee.write_coil(8,0,2)
                        self.conveyor_pub.publish(self.msg_conveyor)
                    if(result.bits[1] == 1):
                        self.flag_stopConveyorF = True
                        self.xbee.write_coil(9,0,2)
                        self.conveyor_pub.publish(self.msg_conveyor)
                except:
                    pass
        else:
            self.openPort()  
    
    def readcheck(self):
        while (self.flag_check == True):
            try:
                countcheck += 1
                coils =  self.xbee.read_coils(self.addres, 1, self.id) 
                if(coils.bits[0]== True):
                    print("thoat")
                    self.flag_check = False
                elif(countcheck >= 5000):
                    self.flag_check = False
                    countcheck = 0
                else:
                    print("guilai")
                    self.xbee.write_coil(self.addres,1,self.id)
                    self.conveyor_pub.publish(self.msg_conveyor)
                    time.sleep(1)
            except:
                pass
            # finally:
            #     rospy.signal_shutdown("Shutdown node AGV1_xbee")

    #############################
    #           MAIN            #
    #############################
    def main(self): 
        self.readData()
        self.readDatalong()
        self.readcheck()
        self.stopConveyor()
        if self.flag_finish >= 2:
            self.flag_finish = 0
            self.flag_stopConveyorF = False
            self.flag_stopConveyorB = False
            #Dung bang tai tren lung
            self.flag_readatashort = False
            self.flag_readatalong = False
            self.msg_conveyor.conveyorF_Run = False
            self.msg_conveyor.conveyorB_Run = False
            self.conveyor_pub.publish(self.msg_conveyor)  
            
            self.msg_xbeeConveyor.status = "finish"
            self.msg_xbeeConveyor.enable = False
            self.xbeeConveyor_pub.publish(self.msg_xbeeConveyor)           

if __name__ == '__main__':
    """ main """
    rospy.init_node('AGV1_XBEE')
    nodename = rospy.get_name()
    rospy.loginfo("%s started" %nodename)

    agv1_xbee = Node_Xbee()
    try:
        rate = rospy.Rate(agv1_xbee.rate) 
        while not rospy.is_shutdown():
            agv1_xbee.main()
            rate.sleep()

    except rospy.ROSInternalException:
        print("\n Node Xbee ERROR \n")
    finally:
        agv1_xbee.closePort()