# CMake generated Testfile for 
# Source directory: /home/intechAGV/catkin_ws/src
# Build directory: /home/intechAGV/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("agv_base_controler")
subdirs("agv_followline")
subdirs("agv_gpio")
subdirs("agv_master")
subdirs("agv_rfid")
subdirs("agv_xbee")
subdirs("beginner_tutorials")
