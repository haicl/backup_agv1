set(_CATKIN_CURRENT_PACKAGE "agv_xbee")
set(agv_xbee_VERSION "0.0.0")
set(agv_xbee_MAINTAINER "intechAGV <intechAGV@todo.todo>")
set(agv_xbee_PACKAGE_FORMAT "2")
set(agv_xbee_BUILD_DEPENDS "roscpp" "rospy" "std_msgs" "message_generation")
set(agv_xbee_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "std_msgs")
set(agv_xbee_BUILDTOOL_DEPENDS "catkin")
set(agv_xbee_BUILDTOOL_EXPORT_DEPENDS )
set(agv_xbee_EXEC_DEPENDS "roscpp" "rospy" "std_msgs" "message_runtime")
set(agv_xbee_RUN_DEPENDS "roscpp" "rospy" "std_msgs" "message_runtime")
set(agv_xbee_TEST_DEPENDS )
set(agv_xbee_DOC_DEPENDS )
set(agv_xbee_URL_WEBSITE "")
set(agv_xbee_URL_BUGTRACKER "")
set(agv_xbee_URL_REPOSITORY "")
set(agv_xbee_DEPRECATED "")