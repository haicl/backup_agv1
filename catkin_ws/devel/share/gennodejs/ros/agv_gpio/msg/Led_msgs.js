// Auto-generated. Do not edit!

// (in-package agv_gpio.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Led_msgs {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.color = null;
      this.ledF_left = null;
      this.ledF_right = null;
      this.ledB_left = null;
      this.ledB_right = null;
    }
    else {
      if (initObj.hasOwnProperty('color')) {
        this.color = initObj.color
      }
      else {
        this.color = '';
      }
      if (initObj.hasOwnProperty('ledF_left')) {
        this.ledF_left = initObj.ledF_left
      }
      else {
        this.ledF_left = false;
      }
      if (initObj.hasOwnProperty('ledF_right')) {
        this.ledF_right = initObj.ledF_right
      }
      else {
        this.ledF_right = false;
      }
      if (initObj.hasOwnProperty('ledB_left')) {
        this.ledB_left = initObj.ledB_left
      }
      else {
        this.ledB_left = false;
      }
      if (initObj.hasOwnProperty('ledB_right')) {
        this.ledB_right = initObj.ledB_right
      }
      else {
        this.ledB_right = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Led_msgs
    // Serialize message field [color]
    bufferOffset = _serializer.string(obj.color, buffer, bufferOffset);
    // Serialize message field [ledF_left]
    bufferOffset = _serializer.bool(obj.ledF_left, buffer, bufferOffset);
    // Serialize message field [ledF_right]
    bufferOffset = _serializer.bool(obj.ledF_right, buffer, bufferOffset);
    // Serialize message field [ledB_left]
    bufferOffset = _serializer.bool(obj.ledB_left, buffer, bufferOffset);
    // Serialize message field [ledB_right]
    bufferOffset = _serializer.bool(obj.ledB_right, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Led_msgs
    let len;
    let data = new Led_msgs(null);
    // Deserialize message field [color]
    data.color = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [ledF_left]
    data.ledF_left = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ledF_right]
    data.ledF_right = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ledB_left]
    data.ledB_left = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ledB_right]
    data.ledB_right = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += _getByteLength(object.color);
    return length + 8;
  }

  static datatype() {
    // Returns string type for a message object
    return 'agv_gpio/Led_msgs';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '450e71ad35f5987b3cdbbe0b91d90d0b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string color
    
    bool ledF_left
    bool ledF_right
    bool ledB_left
    bool ledB_right
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Led_msgs(null);
    if (msg.color !== undefined) {
      resolved.color = msg.color;
    }
    else {
      resolved.color = ''
    }

    if (msg.ledF_left !== undefined) {
      resolved.ledF_left = msg.ledF_left;
    }
    else {
      resolved.ledF_left = false
    }

    if (msg.ledF_right !== undefined) {
      resolved.ledF_right = msg.ledF_right;
    }
    else {
      resolved.ledF_right = false
    }

    if (msg.ledB_left !== undefined) {
      resolved.ledB_left = msg.ledB_left;
    }
    else {
      resolved.ledB_left = false
    }

    if (msg.ledB_right !== undefined) {
      resolved.ledB_right = msg.ledB_right;
    }
    else {
      resolved.ledB_right = false
    }

    return resolved;
    }
};

module.exports = Led_msgs;
