// Auto-generated. Do not edit!

// (in-package agv_gpio.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Safety_msgs {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.sonar_cm = null;
    }
    else {
      if (initObj.hasOwnProperty('sonar_cm')) {
        this.sonar_cm = initObj.sonar_cm
      }
      else {
        this.sonar_cm = new Array(4).fill(0);
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Safety_msgs
    // Check that the constant length array field [sonar_cm] has the right length
    if (obj.sonar_cm.length !== 4) {
      throw new Error('Unable to serialize array field sonar_cm - length must be 4')
    }
    // Serialize message field [sonar_cm]
    bufferOffset = _arraySerializer.int8(obj.sonar_cm, buffer, bufferOffset, 4);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Safety_msgs
    let len;
    let data = new Safety_msgs(null);
    // Deserialize message field [sonar_cm]
    data.sonar_cm = _arrayDeserializer.int8(buffer, bufferOffset, 4)
    return data;
  }

  static getMessageSize(object) {
    return 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'agv_gpio/Safety_msgs';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '12f7c06a1516e3ef5402d7242919303c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int8[4] sonar_cm
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Safety_msgs(null);
    if (msg.sonar_cm !== undefined) {
      resolved.sonar_cm = msg.sonar_cm;
    }
    else {
      resolved.sonar_cm = new Array(4).fill(0)
    }

    return resolved;
    }
};

module.exports = Safety_msgs;
