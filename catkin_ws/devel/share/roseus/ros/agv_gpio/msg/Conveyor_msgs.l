;; Auto-generated. Do not edit!


(when (boundp 'agv_gpio::Conveyor_msgs)
  (if (not (find-package "AGV_GPIO"))
    (make-package "AGV_GPIO"))
  (shadow 'Conveyor_msgs (find-package "AGV_GPIO")))
(unless (find-package "AGV_GPIO::CONVEYOR_MSGS")
  (make-package "AGV_GPIO::CONVEYOR_MSGS"))

(in-package "ROS")
;;//! \htmlinclude Conveyor_msgs.msg.html


(defclass agv_gpio::Conveyor_msgs
  :super ros::object
  :slots (_conveyorF_Run _conveyorF_Reverse _conveyorB_Run _conveyorB_Reverse ))

(defmethod agv_gpio::Conveyor_msgs
  (:init
   (&key
    ((:conveyorF_Run __conveyorF_Run) nil)
    ((:conveyorF_Reverse __conveyorF_Reverse) nil)
    ((:conveyorB_Run __conveyorB_Run) nil)
    ((:conveyorB_Reverse __conveyorB_Reverse) nil)
    )
   (send-super :init)
   (setq _conveyorF_Run __conveyorF_Run)
   (setq _conveyorF_Reverse __conveyorF_Reverse)
   (setq _conveyorB_Run __conveyorB_Run)
   (setq _conveyorB_Reverse __conveyorB_Reverse)
   self)
  (:conveyorF_Run
   (&optional (__conveyorF_Run :null))
   (if (not (eq __conveyorF_Run :null)) (setq _conveyorF_Run __conveyorF_Run)) _conveyorF_Run)
  (:conveyorF_Reverse
   (&optional (__conveyorF_Reverse :null))
   (if (not (eq __conveyorF_Reverse :null)) (setq _conveyorF_Reverse __conveyorF_Reverse)) _conveyorF_Reverse)
  (:conveyorB_Run
   (&optional (__conveyorB_Run :null))
   (if (not (eq __conveyorB_Run :null)) (setq _conveyorB_Run __conveyorB_Run)) _conveyorB_Run)
  (:conveyorB_Reverse
   (&optional (__conveyorB_Reverse :null))
   (if (not (eq __conveyorB_Reverse :null)) (setq _conveyorB_Reverse __conveyorB_Reverse)) _conveyorB_Reverse)
  (:serialization-length
   ()
   (+
    ;; bool _conveyorF_Run
    1
    ;; bool _conveyorF_Reverse
    1
    ;; bool _conveyorB_Run
    1
    ;; bool _conveyorB_Reverse
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _conveyorF_Run
       (if _conveyorF_Run (write-byte -1 s) (write-byte 0 s))
     ;; bool _conveyorF_Reverse
       (if _conveyorF_Reverse (write-byte -1 s) (write-byte 0 s))
     ;; bool _conveyorB_Run
       (if _conveyorB_Run (write-byte -1 s) (write-byte 0 s))
     ;; bool _conveyorB_Reverse
       (if _conveyorB_Reverse (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _conveyorF_Run
     (setq _conveyorF_Run (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _conveyorF_Reverse
     (setq _conveyorF_Reverse (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _conveyorB_Run
     (setq _conveyorB_Run (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _conveyorB_Reverse
     (setq _conveyorB_Reverse (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get agv_gpio::Conveyor_msgs :md5sum-) "111548c16238592512d635263bd304c3")
(setf (get agv_gpio::Conveyor_msgs :datatype-) "agv_gpio/Conveyor_msgs")
(setf (get agv_gpio::Conveyor_msgs :definition-)
      "bool conveyorF_Run
bool conveyorF_Reverse
bool conveyorB_Run
bool conveyorB_Reverse

")



(provide :agv_gpio/Conveyor_msgs "111548c16238592512d635263bd304c3")


