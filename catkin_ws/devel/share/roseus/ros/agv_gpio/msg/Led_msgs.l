;; Auto-generated. Do not edit!


(when (boundp 'agv_gpio::Led_msgs)
  (if (not (find-package "AGV_GPIO"))
    (make-package "AGV_GPIO"))
  (shadow 'Led_msgs (find-package "AGV_GPIO")))
(unless (find-package "AGV_GPIO::LED_MSGS")
  (make-package "AGV_GPIO::LED_MSGS"))

(in-package "ROS")
;;//! \htmlinclude Led_msgs.msg.html


(defclass agv_gpio::Led_msgs
  :super ros::object
  :slots (_color _ledF_left _ledF_right _ledB_left _ledB_right ))

(defmethod agv_gpio::Led_msgs
  (:init
   (&key
    ((:color __color) "")
    ((:ledF_left __ledF_left) nil)
    ((:ledF_right __ledF_right) nil)
    ((:ledB_left __ledB_left) nil)
    ((:ledB_right __ledB_right) nil)
    )
   (send-super :init)
   (setq _color (string __color))
   (setq _ledF_left __ledF_left)
   (setq _ledF_right __ledF_right)
   (setq _ledB_left __ledB_left)
   (setq _ledB_right __ledB_right)
   self)
  (:color
   (&optional __color)
   (if __color (setq _color __color)) _color)
  (:ledF_left
   (&optional (__ledF_left :null))
   (if (not (eq __ledF_left :null)) (setq _ledF_left __ledF_left)) _ledF_left)
  (:ledF_right
   (&optional (__ledF_right :null))
   (if (not (eq __ledF_right :null)) (setq _ledF_right __ledF_right)) _ledF_right)
  (:ledB_left
   (&optional (__ledB_left :null))
   (if (not (eq __ledB_left :null)) (setq _ledB_left __ledB_left)) _ledB_left)
  (:ledB_right
   (&optional (__ledB_right :null))
   (if (not (eq __ledB_right :null)) (setq _ledB_right __ledB_right)) _ledB_right)
  (:serialization-length
   ()
   (+
    ;; string _color
    4 (length _color)
    ;; bool _ledF_left
    1
    ;; bool _ledF_right
    1
    ;; bool _ledB_left
    1
    ;; bool _ledB_right
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _color
       (write-long (length _color) s) (princ _color s)
     ;; bool _ledF_left
       (if _ledF_left (write-byte -1 s) (write-byte 0 s))
     ;; bool _ledF_right
       (if _ledF_right (write-byte -1 s) (write-byte 0 s))
     ;; bool _ledB_left
       (if _ledB_left (write-byte -1 s) (write-byte 0 s))
     ;; bool _ledB_right
       (if _ledB_right (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _color
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _color (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _ledF_left
     (setq _ledF_left (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ledF_right
     (setq _ledF_right (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ledB_left
     (setq _ledB_left (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ledB_right
     (setq _ledB_right (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get agv_gpio::Led_msgs :md5sum-) "450e71ad35f5987b3cdbbe0b91d90d0b")
(setf (get agv_gpio::Led_msgs :datatype-) "agv_gpio/Led_msgs")
(setf (get agv_gpio::Led_msgs :definition-)
      "string color

bool ledF_left
bool ledF_right
bool ledB_left
bool ledB_right

")



(provide :agv_gpio/Led_msgs "450e71ad35f5987b3cdbbe0b91d90d0b")


