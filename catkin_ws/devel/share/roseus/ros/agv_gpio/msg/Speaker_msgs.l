;; Auto-generated. Do not edit!


(when (boundp 'agv_gpio::Speaker_msgs)
  (if (not (find-package "AGV_GPIO"))
    (make-package "AGV_GPIO"))
  (shadow 'Speaker_msgs (find-package "AGV_GPIO")))
(unless (find-package "AGV_GPIO::SPEAKER_MSGS")
  (make-package "AGV_GPIO::SPEAKER_MSGS"))

(in-package "ROS")
;;//! \htmlinclude Speaker_msgs.msg.html


(defclass agv_gpio::Speaker_msgs
  :super ros::object
  :slots (_mode _run ))

(defmethod agv_gpio::Speaker_msgs
  (:init
   (&key
    ((:mode __mode) nil)
    ((:run __run) nil)
    )
   (send-super :init)
   (setq _mode __mode)
   (setq _run __run)
   self)
  (:mode
   (&optional (__mode :null))
   (if (not (eq __mode :null)) (setq _mode __mode)) _mode)
  (:run
   (&optional (__run :null))
   (if (not (eq __run :null)) (setq _run __run)) _run)
  (:serialization-length
   ()
   (+
    ;; bool _mode
    1
    ;; bool _run
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _mode
       (if _mode (write-byte -1 s) (write-byte 0 s))
     ;; bool _run
       (if _run (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _mode
     (setq _mode (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _run
     (setq _run (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get agv_gpio::Speaker_msgs :md5sum-) "3022ae09200e24295c42c33070465986")
(setf (get agv_gpio::Speaker_msgs :datatype-) "agv_gpio/Speaker_msgs")
(setf (get agv_gpio::Speaker_msgs :definition-)
      "bool mode
bool run


")



(provide :agv_gpio/Speaker_msgs "3022ae09200e24295c42c33070465986")


