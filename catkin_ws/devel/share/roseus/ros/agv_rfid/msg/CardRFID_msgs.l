;; Auto-generated. Do not edit!


(when (boundp 'agv_rfid::CardRFID_msgs)
  (if (not (find-package "AGV_RFID"))
    (make-package "AGV_RFID"))
  (shadow 'CardRFID_msgs (find-package "AGV_RFID")))
(unless (find-package "AGV_RFID::CARDRFID_MSGS")
  (make-package "AGV_RFID::CARDRFID_MSGS"))

(in-package "ROS")
;;//! \htmlinclude CardRFID_msgs.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass agv_rfid::CardRFID_msgs
  :super ros::object
  :slots (_header _status _new_cardRFID _old_cardRFID ))

(defmethod agv_rfid::CardRFID_msgs
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:status __status) "")
    ((:new_cardRFID __new_cardRFID) 0)
    ((:old_cardRFID __old_cardRFID) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _status (string __status))
   (setq _new_cardRFID (round __new_cardRFID))
   (setq _old_cardRFID (round __old_cardRFID))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:new_cardRFID
   (&optional __new_cardRFID)
   (if __new_cardRFID (setq _new_cardRFID __new_cardRFID)) _new_cardRFID)
  (:old_cardRFID
   (&optional __old_cardRFID)
   (if __old_cardRFID (setq _old_cardRFID __old_cardRFID)) _old_cardRFID)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; string _status
    4 (length _status)
    ;; int8 _new_cardRFID
    1
    ;; int8 _old_cardRFID
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; string _status
       (write-long (length _status) s) (princ _status s)
     ;; int8 _new_cardRFID
       (write-byte _new_cardRFID s)
     ;; int8 _old_cardRFID
       (write-byte _old_cardRFID s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; string _status
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _status (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int8 _new_cardRFID
     (setq _new_cardRFID (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _new_cardRFID 127) (setq _new_cardRFID (- _new_cardRFID 256)))
   ;; int8 _old_cardRFID
     (setq _old_cardRFID (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _old_cardRFID 127) (setq _old_cardRFID (- _old_cardRFID 256)))
   ;;
   self)
  )

(setf (get agv_rfid::CardRFID_msgs :md5sum-) "2e1f9135a761a98dae7de78f5cbc1da8")
(setf (get agv_rfid::CardRFID_msgs :datatype-) "agv_rfid/CardRFID_msgs")
(setf (get agv_rfid::CardRFID_msgs :definition-)
      "Header header

string status
int8 new_cardRFID
int8 old_cardRFID


================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :agv_rfid/CardRFID_msgs "2e1f9135a761a98dae7de78f5cbc1da8")


