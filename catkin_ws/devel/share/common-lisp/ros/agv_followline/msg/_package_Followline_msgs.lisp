(cl:in-package agv_followline-msg)
(cl:export '(HEADER-VAL
          HEADER
          FORWARD-VAL
          FORWARD
          BACKWARD-VAL
          BACKWARD
))