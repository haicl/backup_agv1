; Auto-generated. Do not edit!


(cl:in-package agv_followline-msg)


;//! \htmlinclude Followline_msgs.msg.html

(cl:defclass <Followline_msgs> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (forward
    :reader forward
    :initarg :forward
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 16 :element-type 'cl:fixnum :initial-element 0))
   (backward
    :reader backward
    :initarg :backward
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 16 :element-type 'cl:fixnum :initial-element 0)))
)

(cl:defclass Followline_msgs (<Followline_msgs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Followline_msgs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Followline_msgs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name agv_followline-msg:<Followline_msgs> is deprecated: use agv_followline-msg:Followline_msgs instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Followline_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_followline-msg:header-val is deprecated.  Use agv_followline-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'forward-val :lambda-list '(m))
(cl:defmethod forward-val ((m <Followline_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_followline-msg:forward-val is deprecated.  Use agv_followline-msg:forward instead.")
  (forward m))

(cl:ensure-generic-function 'backward-val :lambda-list '(m))
(cl:defmethod backward-val ((m <Followline_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_followline-msg:backward-val is deprecated.  Use agv_followline-msg:backward instead.")
  (backward m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Followline_msgs>) ostream)
  "Serializes a message object of type '<Followline_msgs>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    ))
   (cl:slot-value msg 'forward))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    ))
   (cl:slot-value msg 'backward))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Followline_msgs>) istream)
  "Deserializes a message object of type '<Followline_msgs>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:setf (cl:slot-value msg 'forward) (cl:make-array 16))
  (cl:let ((vals (cl:slot-value msg 'forward)))
    (cl:dotimes (i 16)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))))
  (cl:setf (cl:slot-value msg 'backward) (cl:make-array 16))
  (cl:let ((vals (cl:slot-value msg 'backward)))
    (cl:dotimes (i 16)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Followline_msgs>)))
  "Returns string type for a message object of type '<Followline_msgs>"
  "agv_followline/Followline_msgs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Followline_msgs)))
  "Returns string type for a message object of type 'Followline_msgs"
  "agv_followline/Followline_msgs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Followline_msgs>)))
  "Returns md5sum for a message object of type '<Followline_msgs>"
  "659069a15bfcad3113211bf07cd85a39")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Followline_msgs)))
  "Returns md5sum for a message object of type 'Followline_msgs"
  "659069a15bfcad3113211bf07cd85a39")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Followline_msgs>)))
  "Returns full string definition for message of type '<Followline_msgs>"
  (cl:format cl:nil "Header header~%~%int8[16] forward~%int8[16] backward~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Followline_msgs)))
  "Returns full string definition for message of type 'Followline_msgs"
  (cl:format cl:nil "Header header~%~%int8[16] forward~%int8[16] backward~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Followline_msgs>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'forward) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'backward) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Followline_msgs>))
  "Converts a ROS message object to a list"
  (cl:list 'Followline_msgs
    (cl:cons ':header (header msg))
    (cl:cons ':forward (forward msg))
    (cl:cons ':backward (backward msg))
))
