(cl:in-package agv_rfid-msg)
(cl:export '(HEADER-VAL
          HEADER
          STATUS-VAL
          STATUS
          NEW_CARDRFID-VAL
          NEW_CARDRFID
          OLD_CARDRFID-VAL
          OLD_CARDRFID
))