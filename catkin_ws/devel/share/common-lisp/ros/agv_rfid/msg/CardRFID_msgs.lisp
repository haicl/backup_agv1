; Auto-generated. Do not edit!


(cl:in-package agv_rfid-msg)


;//! \htmlinclude CardRFID_msgs.msg.html

(cl:defclass <CardRFID_msgs> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (status
    :reader status
    :initarg :status
    :type cl:string
    :initform "")
   (new_cardRFID
    :reader new_cardRFID
    :initarg :new_cardRFID
    :type cl:fixnum
    :initform 0)
   (old_cardRFID
    :reader old_cardRFID
    :initarg :old_cardRFID
    :type cl:fixnum
    :initform 0))
)

(cl:defclass CardRFID_msgs (<CardRFID_msgs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <CardRFID_msgs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'CardRFID_msgs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name agv_rfid-msg:<CardRFID_msgs> is deprecated: use agv_rfid-msg:CardRFID_msgs instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <CardRFID_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_rfid-msg:header-val is deprecated.  Use agv_rfid-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <CardRFID_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_rfid-msg:status-val is deprecated.  Use agv_rfid-msg:status instead.")
  (status m))

(cl:ensure-generic-function 'new_cardRFID-val :lambda-list '(m))
(cl:defmethod new_cardRFID-val ((m <CardRFID_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_rfid-msg:new_cardRFID-val is deprecated.  Use agv_rfid-msg:new_cardRFID instead.")
  (new_cardRFID m))

(cl:ensure-generic-function 'old_cardRFID-val :lambda-list '(m))
(cl:defmethod old_cardRFID-val ((m <CardRFID_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_rfid-msg:old_cardRFID-val is deprecated.  Use agv_rfid-msg:old_cardRFID instead.")
  (old_cardRFID m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <CardRFID_msgs>) ostream)
  "Serializes a message object of type '<CardRFID_msgs>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'status))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'status))
  (cl:let* ((signed (cl:slot-value msg 'new_cardRFID)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'old_cardRFID)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <CardRFID_msgs>) istream)
  "Deserializes a message object of type '<CardRFID_msgs>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'status) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'status) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'new_cardRFID) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'old_cardRFID) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<CardRFID_msgs>)))
  "Returns string type for a message object of type '<CardRFID_msgs>"
  "agv_rfid/CardRFID_msgs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'CardRFID_msgs)))
  "Returns string type for a message object of type 'CardRFID_msgs"
  "agv_rfid/CardRFID_msgs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<CardRFID_msgs>)))
  "Returns md5sum for a message object of type '<CardRFID_msgs>"
  "2e1f9135a761a98dae7de78f5cbc1da8")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'CardRFID_msgs)))
  "Returns md5sum for a message object of type 'CardRFID_msgs"
  "2e1f9135a761a98dae7de78f5cbc1da8")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<CardRFID_msgs>)))
  "Returns full string definition for message of type '<CardRFID_msgs>"
  (cl:format cl:nil "Header header~%~%string status~%int8 new_cardRFID~%int8 old_cardRFID~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'CardRFID_msgs)))
  "Returns full string definition for message of type 'CardRFID_msgs"
  (cl:format cl:nil "Header header~%~%string status~%int8 new_cardRFID~%int8 old_cardRFID~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <CardRFID_msgs>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'status))
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <CardRFID_msgs>))
  "Converts a ROS message object to a list"
  (cl:list 'CardRFID_msgs
    (cl:cons ':header (header msg))
    (cl:cons ':status (status msg))
    (cl:cons ':new_cardRFID (new_cardRFID msg))
    (cl:cons ':old_cardRFID (old_cardRFID msg))
))
