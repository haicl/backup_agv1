
(cl:in-package :asdf)

(defsystem "agv_rfid-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "CardRFID_msgs" :depends-on ("_package_CardRFID_msgs"))
    (:file "_package_CardRFID_msgs" :depends-on ("_package"))
  ))