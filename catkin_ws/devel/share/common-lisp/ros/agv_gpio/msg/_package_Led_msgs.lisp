(cl:in-package agv_gpio-msg)
(cl:export '(COLOR-VAL
          COLOR
          LEDF_LEFT-VAL
          LEDF_LEFT
          LEDF_RIGHT-VAL
          LEDF_RIGHT
          LEDB_LEFT-VAL
          LEDB_LEFT
          LEDB_RIGHT-VAL
          LEDB_RIGHT
))