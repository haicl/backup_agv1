; Auto-generated. Do not edit!


(cl:in-package agv_gpio-msg)


;//! \htmlinclude Led_msgs.msg.html

(cl:defclass <Led_msgs> (roslisp-msg-protocol:ros-message)
  ((color
    :reader color
    :initarg :color
    :type cl:string
    :initform "")
   (ledF_left
    :reader ledF_left
    :initarg :ledF_left
    :type cl:boolean
    :initform cl:nil)
   (ledF_right
    :reader ledF_right
    :initarg :ledF_right
    :type cl:boolean
    :initform cl:nil)
   (ledB_left
    :reader ledB_left
    :initarg :ledB_left
    :type cl:boolean
    :initform cl:nil)
   (ledB_right
    :reader ledB_right
    :initarg :ledB_right
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Led_msgs (<Led_msgs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Led_msgs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Led_msgs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name agv_gpio-msg:<Led_msgs> is deprecated: use agv_gpio-msg:Led_msgs instead.")))

(cl:ensure-generic-function 'color-val :lambda-list '(m))
(cl:defmethod color-val ((m <Led_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_gpio-msg:color-val is deprecated.  Use agv_gpio-msg:color instead.")
  (color m))

(cl:ensure-generic-function 'ledF_left-val :lambda-list '(m))
(cl:defmethod ledF_left-val ((m <Led_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_gpio-msg:ledF_left-val is deprecated.  Use agv_gpio-msg:ledF_left instead.")
  (ledF_left m))

(cl:ensure-generic-function 'ledF_right-val :lambda-list '(m))
(cl:defmethod ledF_right-val ((m <Led_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_gpio-msg:ledF_right-val is deprecated.  Use agv_gpio-msg:ledF_right instead.")
  (ledF_right m))

(cl:ensure-generic-function 'ledB_left-val :lambda-list '(m))
(cl:defmethod ledB_left-val ((m <Led_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_gpio-msg:ledB_left-val is deprecated.  Use agv_gpio-msg:ledB_left instead.")
  (ledB_left m))

(cl:ensure-generic-function 'ledB_right-val :lambda-list '(m))
(cl:defmethod ledB_right-val ((m <Led_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_gpio-msg:ledB_right-val is deprecated.  Use agv_gpio-msg:ledB_right instead.")
  (ledB_right m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Led_msgs>) ostream)
  "Serializes a message object of type '<Led_msgs>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'color))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'color))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ledF_left) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ledF_right) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ledB_left) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ledB_right) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Led_msgs>) istream)
  "Deserializes a message object of type '<Led_msgs>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'color) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'color) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'ledF_left) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ledF_right) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ledB_left) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ledB_right) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Led_msgs>)))
  "Returns string type for a message object of type '<Led_msgs>"
  "agv_gpio/Led_msgs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Led_msgs)))
  "Returns string type for a message object of type 'Led_msgs"
  "agv_gpio/Led_msgs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Led_msgs>)))
  "Returns md5sum for a message object of type '<Led_msgs>"
  "450e71ad35f5987b3cdbbe0b91d90d0b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Led_msgs)))
  "Returns md5sum for a message object of type 'Led_msgs"
  "450e71ad35f5987b3cdbbe0b91d90d0b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Led_msgs>)))
  "Returns full string definition for message of type '<Led_msgs>"
  (cl:format cl:nil "string color~%~%bool ledF_left~%bool ledF_right~%bool ledB_left~%bool ledB_right~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Led_msgs)))
  "Returns full string definition for message of type 'Led_msgs"
  (cl:format cl:nil "string color~%~%bool ledF_left~%bool ledF_right~%bool ledB_left~%bool ledB_right~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Led_msgs>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'color))
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Led_msgs>))
  "Converts a ROS message object to a list"
  (cl:list 'Led_msgs
    (cl:cons ':color (color msg))
    (cl:cons ':ledF_left (ledF_left msg))
    (cl:cons ':ledF_right (ledF_right msg))
    (cl:cons ':ledB_left (ledB_left msg))
    (cl:cons ':ledB_right (ledB_right msg))
))
