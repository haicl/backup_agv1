; Auto-generated. Do not edit!


(cl:in-package agv_gpio-msg)


;//! \htmlinclude Safety_msgs.msg.html

(cl:defclass <Safety_msgs> (roslisp-msg-protocol:ros-message)
  ((sonar_cm
    :reader sonar_cm
    :initarg :sonar_cm
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 4 :element-type 'cl:fixnum :initial-element 0)))
)

(cl:defclass Safety_msgs (<Safety_msgs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Safety_msgs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Safety_msgs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name agv_gpio-msg:<Safety_msgs> is deprecated: use agv_gpio-msg:Safety_msgs instead.")))

(cl:ensure-generic-function 'sonar_cm-val :lambda-list '(m))
(cl:defmethod sonar_cm-val ((m <Safety_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_gpio-msg:sonar_cm-val is deprecated.  Use agv_gpio-msg:sonar_cm instead.")
  (sonar_cm m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Safety_msgs>) ostream)
  "Serializes a message object of type '<Safety_msgs>"
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    ))
   (cl:slot-value msg 'sonar_cm))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Safety_msgs>) istream)
  "Deserializes a message object of type '<Safety_msgs>"
  (cl:setf (cl:slot-value msg 'sonar_cm) (cl:make-array 4))
  (cl:let ((vals (cl:slot-value msg 'sonar_cm)))
    (cl:dotimes (i 4)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Safety_msgs>)))
  "Returns string type for a message object of type '<Safety_msgs>"
  "agv_gpio/Safety_msgs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Safety_msgs)))
  "Returns string type for a message object of type 'Safety_msgs"
  "agv_gpio/Safety_msgs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Safety_msgs>)))
  "Returns md5sum for a message object of type '<Safety_msgs>"
  "12f7c06a1516e3ef5402d7242919303c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Safety_msgs)))
  "Returns md5sum for a message object of type 'Safety_msgs"
  "12f7c06a1516e3ef5402d7242919303c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Safety_msgs>)))
  "Returns full string definition for message of type '<Safety_msgs>"
  (cl:format cl:nil "int8[4] sonar_cm~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Safety_msgs)))
  "Returns full string definition for message of type 'Safety_msgs"
  (cl:format cl:nil "int8[4] sonar_cm~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Safety_msgs>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'sonar_cm) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Safety_msgs>))
  "Converts a ROS message object to a list"
  (cl:list 'Safety_msgs
    (cl:cons ':sonar_cm (sonar_cm msg))
))
