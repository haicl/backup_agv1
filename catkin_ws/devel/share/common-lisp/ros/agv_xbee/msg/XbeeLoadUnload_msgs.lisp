; Auto-generated. Do not edit!


(cl:in-package agv_xbee-msg)


;//! \htmlinclude XbeeLoadUnload_msgs.msg.html

(cl:defclass <XbeeLoadUnload_msgs> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (status
    :reader status
    :initarg :status
    :type cl:string
    :initform "")
   (enable
    :reader enable
    :initarg :enable
    :type cl:boolean
    :initform cl:nil)
   (station_selection
    :reader station_selection
    :initarg :station_selection
    :type cl:string
    :initform "")
   (case
    :reader case
    :initarg :case
    :type cl:fixnum
    :initform 0))
)

(cl:defclass XbeeLoadUnload_msgs (<XbeeLoadUnload_msgs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <XbeeLoadUnload_msgs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'XbeeLoadUnload_msgs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name agv_xbee-msg:<XbeeLoadUnload_msgs> is deprecated: use agv_xbee-msg:XbeeLoadUnload_msgs instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <XbeeLoadUnload_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_xbee-msg:header-val is deprecated.  Use agv_xbee-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <XbeeLoadUnload_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_xbee-msg:status-val is deprecated.  Use agv_xbee-msg:status instead.")
  (status m))

(cl:ensure-generic-function 'enable-val :lambda-list '(m))
(cl:defmethod enable-val ((m <XbeeLoadUnload_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_xbee-msg:enable-val is deprecated.  Use agv_xbee-msg:enable instead.")
  (enable m))

(cl:ensure-generic-function 'station_selection-val :lambda-list '(m))
(cl:defmethod station_selection-val ((m <XbeeLoadUnload_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_xbee-msg:station_selection-val is deprecated.  Use agv_xbee-msg:station_selection instead.")
  (station_selection m))

(cl:ensure-generic-function 'case-val :lambda-list '(m))
(cl:defmethod case-val ((m <XbeeLoadUnload_msgs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader agv_xbee-msg:case-val is deprecated.  Use agv_xbee-msg:case instead.")
  (case m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <XbeeLoadUnload_msgs>) ostream)
  "Serializes a message object of type '<XbeeLoadUnload_msgs>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'status))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'status))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'enable) 1 0)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'station_selection))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'station_selection))
  (cl:let* ((signed (cl:slot-value msg 'case)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <XbeeLoadUnload_msgs>) istream)
  "Deserializes a message object of type '<XbeeLoadUnload_msgs>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'status) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'status) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'enable) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'station_selection) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'station_selection) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'case) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<XbeeLoadUnload_msgs>)))
  "Returns string type for a message object of type '<XbeeLoadUnload_msgs>"
  "agv_xbee/XbeeLoadUnload_msgs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'XbeeLoadUnload_msgs)))
  "Returns string type for a message object of type 'XbeeLoadUnload_msgs"
  "agv_xbee/XbeeLoadUnload_msgs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<XbeeLoadUnload_msgs>)))
  "Returns md5sum for a message object of type '<XbeeLoadUnload_msgs>"
  "76da4dc3ed4b4e3d47ad7c8dd9521bed")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'XbeeLoadUnload_msgs)))
  "Returns md5sum for a message object of type 'XbeeLoadUnload_msgs"
  "76da4dc3ed4b4e3d47ad7c8dd9521bed")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<XbeeLoadUnload_msgs>)))
  "Returns full string definition for message of type '<XbeeLoadUnload_msgs>"
  (cl:format cl:nil "Header header~%~%string status~%bool enable~%string station_selection~%int8 case~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'XbeeLoadUnload_msgs)))
  "Returns full string definition for message of type 'XbeeLoadUnload_msgs"
  (cl:format cl:nil "Header header~%~%string status~%bool enable~%string station_selection~%int8 case~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <XbeeLoadUnload_msgs>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'status))
     1
     4 (cl:length (cl:slot-value msg 'station_selection))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <XbeeLoadUnload_msgs>))
  "Converts a ROS message object to a list"
  (cl:list 'XbeeLoadUnload_msgs
    (cl:cons ':header (header msg))
    (cl:cons ':status (status msg))
    (cl:cons ':enable (enable msg))
    (cl:cons ':station_selection (station_selection msg))
    (cl:cons ':case (case msg))
))
