
(cl:in-package :asdf)

(defsystem "agv_xbee-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "XbeeLoadUnload_msgs" :depends-on ("_package_XbeeLoadUnload_msgs"))
    (:file "_package_XbeeLoadUnload_msgs" :depends-on ("_package"))
  ))